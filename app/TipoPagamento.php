<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TipoPagamento extends Model
{
    protected $table = "tipo_pagamento";
    public $fillable = ['codigo', 'descricao', 'usuario_id_fk'];

    //verifica se já existe codigo cadastrado
   public function existeCodigocadastrado($codigo, $id){

    if($id){//update

        $existe = DB::table('tipo_pagamento')->where([
            ['codigo', '=', $codigo],
            ['id', '<>', $id],
        ])->count();

    }else{//store
        $existe = DB::table('tipo_pagamento')->where([
            ['codigo', '=', $codigo],
        ])->count();
    }

    if($existe > 0){
        return true;
    }else{
        return false;
    }

}
}
