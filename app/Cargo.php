<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $table='cargos';
    public $fillable =['codigo','cargo','quadro','cargo_genericos_id'];
}
