<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lotacao extends Model
{

    use SoftDeletes;

    protected $table = "lotacoes";
    public $fillable = ['codigo', 'descricao', 'usuario_id_fk'];

   //verifica se já existe codigo cadastrado
   public function existeCodigocadastrado($codigo, $id){

    if($id){//update

        $existe = DB::table('lotacoes')->where([
            ['codigo', '=', $codigo],
            ['id', '<>', $id],
        ])->count();

    }else{//store
        $existe = DB::table('lotacoes')->where([
            ['codigo', '=', $codigo],
        ])->count();
    }

    if($existe > 0){
        return true;
    }else{
        return false;
    }

    }
}
