<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public static function userLogadoComCpf(){

        $user_cpf = Auth::user()->cpf;

        if($user_cpf){
            return true;
        }else{
            return false;
        }
    }

    //usuário logado vinculado a funcionário?
    public static function userLogadoVinculadoFunc(){

        $user_cpf = Auth::user()->cpf;

        $funcionario = Funcionario::where('cpf',  $user_cpf)->orderByDesc('id')->first();

        if($funcionario){
            return true;
        }else{
            return false;
        }

    }

    //get funcionario vinculado a usuario
    public static function getFuncionarioUser($user_cpf){
        $funcionario = Funcionario::where('cpf',  $user_cpf)->orderByDesc('id')->first();
        return $funcionario;
    }

    public static function getFuncionarioUserLogado(){

        $user_cpf = Auth::user()->cpf;

        $funcionario = User::getFuncionarioUser($user_cpf);
        return $funcionario;
    }
}
