<?php

namespace App\Utils;


use DateTime;
use DateTimeZone;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

/**
 * Class DateUtil - classe que com metodos de util de data
 * @package Intra\Utils
 */
class DateUtil {
    public static function getMonths() {
        return [['id' => '1', 'name' => 'Janeiro'],
                ['id' => '2', 'name' => 'Fevereiro'],
                ['id' => '3', 'name' => 'Março'],
                ['id' => '4', 'name' => 'Abril'],
                ['id' => '5', 'name' => 'Maio'],
                ['id' => '6', 'name' => 'Junho'],
                ['id' => '7', 'name' => 'Julho'],
                ['id' => '8', 'name' => 'Agosto'],
                ['id' => '9', 'name' => 'Setembro'],
                ['id' => '10', 'name' => 'Outubro'],
                ['id' => '11', 'name' => 'Novembro'],
                ['id' => '12', 'name' => 'Dezembro']
                ];
    }

    public static function getCurrentMonth() {
        $now = new DateTime('now');
        $month = $now->format('m');
        return $month;
    }

    public static function getCurrentDay() {
        $now = new DateTime('now');
        $day = $now->format('d');
        return $day;
    }

    public static function formatDateTime($stringDate) {
        $date = new DateTime($stringDate);

        return $date->format('d/m/Y H:i');
    }

    public static function formatDate($stringDate) {
        $date = new DateTime($sringDate);
        return $date->format('d/m/Y');
    }

    public static function formatDateAmericano($stringDate) {
        $date = new DateTime($stringDate);
        return $date->format('Y-m-d');
    }

    public static function formatDay($stringDate) {
        $date = new DateTime($stringDate);
        return $date->format('d');
    }

    public static function formatTime($stringDate) {
        $date = new DateTime($stringDate);
        return $date->format('H:i');
    }

    public static function fullDate($stringDate) {
        $date = new DateTime($stringDate);
        return $date->format('Y-m-d H:i:s O');
    }

    public static function getDateDatabaseFromDateForm($dateForm) {
        return date('Y-m-d', strtotime(str_replace('/','-', $dateForm)));
    }

    public static function getDateFormFromDateDatabase($dateDataBase) {
        return date('d/m/Y', strtotime(str_replace('-','/',$dateDataBase)));
    }

    public static function getDatetimeDatabaseFromDatetimeForm($dateForm) {
        return date('Y-m-d H:i', strtotime(str_replace('/','-', $dateForm)));
    }

    public static function getDatetimeFormFromDatetimeDatabase($dateDataBase) {
        return date('d/m/Y H:i', strtotime(str_replace('-','/',$dateDataBase)));
    }

    public static function qtdDias($dataInicio, $dataFim){

        //Calcula a diferença em segundos entre as datas
        $diferenca = (strtotime($dataFim) - strtotime($dataInicio));

        //Calcula a diferença em dias
        $dias = floor( $diferenca / (86400));

         return $dias+1;
    }


}
