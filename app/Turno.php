<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Turno extends Model
{
    use SoftDeletes;

    protected $table = "turnos";
    public $fillable = ['descricao', 'usuario_id_fk'];

   //verifica se já existe codigo cadastrado
   public function existeDescricaocadastrado($descricao, $id){

    if($id){//update

        $existe = DB::table('turnos')->where([
            ['descricao', '=', $descricao],
            ['id', '<>', $id],
        ])->count();

    }else{//store
        $existe = DB::table('turnos')->where([
            ['descricao', '=', $descricao],
        ])->count();
    }

    if($existe > 0){
        return true;
    }else{
        return false;
    }

    }
}
