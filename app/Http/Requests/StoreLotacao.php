<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLotacao extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'codigo' => 'required|min:17',
            'descricao' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'codigo.required' => 'O campo Código é de preenchimento obrigatório!',
            'descricao.required' => 'O campo Descrição é de preenchimento obrigatório!',
            'codigo.min' => 'O campo CÓDIGO deve ter 13 números', //O RESTO É PONTO
        ];
    }
}
