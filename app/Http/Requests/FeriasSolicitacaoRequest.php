<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FeriasSolicitacaoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
            return [
                'data_inicial' => 'required|date_format:d/m/Y',
                'data_final' => 'required|date_format:d/m/Y|after:data_inicial',
            ];
    }

    public function messages()
    {
        return [
            'data_inicial.date_format' => 'O campo DATA INICIAL não é uma data válida!',
            'data_inicial.required' => 'O campo DATA INICIAL é obrigatório!',
            'data_final.date_format' => 'O campo DATA FINAL não é uma data válida!',
            'data_final.required' => 'O campo DATA FINAL é obrigatório!',
            'data_final' => ['after:data_inicial' => 'O campo DATA FINAL deve ser maior que DATA INICIAL!'],
        ];
    }

}
