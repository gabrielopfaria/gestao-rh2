<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreHorarioTrabalho extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'descricao' => 'required|unique:horario_trabalhos,descricao|max:255',
        ];
    }
    public function messages()
    {
        return [
            'descricao.unique' => 'Horário já cadastrado',
            'descricao.required' => 'O campo Descrição é de preenchimento obrigatório!',
        ];
    }
}
