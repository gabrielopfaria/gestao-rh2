<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FeriasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ano' => 'required',
            'data_expedicao' => 'required|date_format:d/m/Y',
            'func_matricula' => 'required',
            'ano_aquisitivo_inicial' => 'required',
            'ano_aquisitivo_final' => 'required',
            'data_inicial' => 'required|date_format:d/m/Y',
            'data_final' => 'required|date_format:d/m/Y|after:data_inicial',
            'data_retorno' => 'required|date_format:d/m/Y',
            'qtd_dias' => 'required',
            //'usuario_id_fk' => 'required',
        ];
    }

    public function messages()
    {

        return [
            'ano.required'=> 'O campo ANO é obrigatório!',
            'data_expedicao.required' => 'O campo DATA DE EMISSÃO é obrigatório!',
            'data_inicial.date_format' => 'O campo DATA INICIAL não é uma data válida!',
            'data_inicial.required' => 'O campo DATA INICIAL é obrigatório!',
            'data_final.date_format' => 'O campo DATA FINAL não é uma data válida!',
            'data_final.required' => 'O campo DATA FINAL é obrigatório!',
            'data_retorno.date_format' => 'O campo DATA RETORNO não é uma data válida!',
            'data_retorno.required' => 'O campo DATA RETORNO é obrigatório!',
            'qtd_dias.required' => 'O campo QTD. DIAS é obrigatório!',
            'data_final' => ['after:data_inicial' => 'O campo DATA FINAL deve ser maior que DATA INICIAL!'],
            //'usuario_id_fk.required' => 'Usuário logado é obrigatório'
        ];
    }
}
