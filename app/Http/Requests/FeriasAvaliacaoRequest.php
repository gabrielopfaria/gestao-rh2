<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FeriasAvaliacaoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'required',
            'solicitacao_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'status.required' => 'O campo Avaliação é obrigatório!',
            'solicitacao_id.required' => 'O campo Solicitação é obrigatório!',
        ];
    }
}
