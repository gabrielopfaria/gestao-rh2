<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreVinculo;
use App\Http\Requests\UpdateVinculo;
use App\Vinculo;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VinculoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vinculos = Vinculo::all();
       return view('vinculos.index',compact('vinculos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vinculos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVinculo $request)
    {
        DB::beginTransaction();
        $validado = $request->validated();
        $vinculo = Vinculo::create([
            'sigla'=>$validado['sigla'],
            'descricao'=>$validado['descricao'],
            'usuario_id_fk'=>auth()->user()->id
        ]);
        if(!$vinculo){
            DB::rollBack();
            return redirect()->route('vinculo.index')->with('error',"Não foi possível criar o vinculo");
        }
        DB::commit();
        return redirect()->route('vinculo.index')->with('status',"vinculo criado com sucesso");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Vinculo $vinculo)
    {
        return view('vinculos.edit',compact('vinculo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateVinculo $request, Vinculo $vinculo)
    {
        DB::beginTransaction();
        try{
            $vinculo->update($request->validated());
        }catch(Exception $e){
            DB::rollBack();
            return redirect()->route('vinculo.index')->with('error',"Não foi possível alterar o vinculo");
        }
        DB::commit();
        return redirect()->route('vinculo.index')->with('status',"vinculo alterado com sucesso");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
