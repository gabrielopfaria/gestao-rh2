<?php

namespace App\Http\Controllers;

use App\Turno;
use Illuminate\Http\Request;
use Request as rq;

class TurnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $turno = Turno::orderBy('descricao')->withTrashed()->get();
        return view('turno.index', compact('turno'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $turno = new Turno();
        return view('turno.create', compact ('turno'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $turno = new Turno();
        $existeDescricao = $turno->existeDescricaocadastrado($request->descricao, NULL);

        if($existeDescricao){
            rq::session()->flash('status-not', 'Já existe este turno cadastrado.');
            return redirect()->back()->withInput();
        }

        try{

            $turno = Turno::create([
                'descricao' => $request->descricao,
                'usuario_id_fk' => auth()->user()->id
            ]);

            if($turno){
                rq::session()->flash('status', 'Turno cadastrado com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar o turno.');
            }

        }catch(\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar o turno. '.$e->getMessage());
        }

        return redirect()->route('turno.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Turno  $turno
     * @return \Illuminate\Http\Response
     */
    public function show(Turno $turno)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Turno  $turno
     * @return \Illuminate\Http\Response
     */
    public function edit(Turno $turno)
    {
        return view('turno.edit', compact('turno'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Turno  $turno
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Turno $turno)
    {
        $existeDescricao = $turno->existeDescricaocadastrado($request->descricao, $turno->id);

        if($existeDescricao){
            rq::session()->flash('status-not', 'Já existe um turno com esse horário.');
            return redirect()->back()->withInput();
        }

        try{

            $turno = $turno->update([
                'descricao' => $request->descricao,
            ]);

            if($turno){
                rq::session()->flash('status', 'Turno atualizado com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar o turno.');
            }

        }catch(\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar turno. '.$e->getMessage());
        }

        return redirect()->route('turno.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Turno  $turno
     * @return \Illuminate\Http\Response
     */
    public function destroy(Turno $turno)
    {
        //
    }
    //para ativar apos soft delete
    public function ativar($id)
    {
        // Recupera pelo ID
        $turno = Turno::where('id', $id)->withTrashed()->first();

        // Restaura:
        $rest = $turno->restore();

        if($rest){
            rq::session()->flash('status', 'Turno ativado.');
        }else{
            rq::session()->flash('status-not', 'Ocorreu um erro ao ativar turno.');
        }

        return redirect()->route('turno.index');

    }
    //inativar sof delete
    public function delete($id)
    {
        $turno = Turno::findOrFail($id);

        try{
            $turno->delete();
            rq::session()->flash('status', 'Turno '.$turno->descricao.' inativado com sucesso.');
            return redirect()->route('turno.index');
        }
        catch (\Exception $e){

/*
            if($e->getCode() == 23000 ){
                rq::session()->flash('status-not', 'Lotação não foi excluída. A mesma está sendo utilizada em outro cadastro! ');
            }else{
                rq::session()->flash('status-not', 'Lotação não foi excluída! '.$e->getMessage());
            }
*/
            rq::session()->flash('status-not', 'Turno não foi inativado! '.$e->getMessage());

            return redirect()->route('turno.index');
        }

    }
}
