<?php

namespace App\Http\Controllers;

use App\Banco;
use App\Http\Requests\StoreBanco;
use Illuminate\Http\Request;
use Request as rq;

class BancoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bancos = Banco::orderBy('descricao')->get();

        return view('banco.index', compact('bancos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $banco = new Banco();

        return view('banco.formulario', compact('banco'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBanco $request)
    {

        $banco = new Banco();
        $existeCodigo = $banco->existeCodigocadastrado($request->codigo, NULL);

        if($existeCodigo){
            rq::session()->flash('status-not', 'Já existe um banco cadastrado com o código informado.');
            return redirect()->back()->withInput();
        }

        try{

            $banco = Banco::create([
                'codigo' => $request->codigo,
                'descricao' => $request->descricao,
                'usuario_id_fk' => auth()->user()->id
            ]);

            if($banco){
                rq::session()->flash('status', 'Banco cadastrado com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar banco.');
            }

        }catch(\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar banco. '.$e->getMessage());
        }

        return redirect()->route('banco.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banco  $banco
     * @return \Illuminate\Http\Response
     */
    public function show(Banco $banco)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banco  $banco
     * @return \Illuminate\Http\Response
     */
    public function edit(Banco $banco)
    {
        return view('banco.formulario', compact('banco'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banco  $banco
     * @return \Illuminate\Http\Response
     */
    public function update(StoreBanco $request, Banco $banco)
    {

        $existeCodigo = $banco->existeCodigocadastrado($request->codigo, $banco->id);

        if($existeCodigo){
            rq::session()->flash('status-not', 'Já existe um banco cadastrado com o código informado.');
            return redirect()->back()->withInput();
        }

        try{

            $banco = $banco->update([
                'codigo' => $request->codigo,
                'descricao' => $request->descricao,
            ]);

            if($banco){
                rq::session()->flash('status', 'Banco atualizado com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar banco.');
            }

        }catch(\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar banco. '.$e->getMessage());
        }

        return redirect()->route('banco.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banco  $banco
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banco $banco)
    {
        //
    }

    public function delete($id)
    {
        $banco = Banco::findOrFail($id);

        try{
            $banco->delete();
            rq::session()->flash('status', 'Banco '.$banco->descricao.' excluído com sucesso.');
            return redirect()->route('banco.index');
        }
        catch (\Exception $e){

            if($e->getCode() == 23000 ){
                rq::session()->flash('status-not', 'Banco não foi excluído. O mesmo está sendo utilizado em outro cadastro! ');
            }else{
                rq::session()->flash('status-not', 'Banco não foi excluído! '.$e->getMessage());
            }

            return redirect()->route('banco.index');
        }

    }
}
