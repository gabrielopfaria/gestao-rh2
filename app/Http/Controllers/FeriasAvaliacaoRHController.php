<?php

namespace App\Http\Controllers;

use App\Utils\DateUtil;
use App\Ferias;
use App\FeriasSolicitacao;
use App\FeriasAvaliacao;
use App\Funcionario;
use App\Http\Requests\FeriasAvaliacaoRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Request as rq;
use Yajra\DataTables\DataTables;

class FeriasAvaliacaoRHController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //lista de ferias para avaliação
    public function index()
    {
        return view('ferias.aval_rh.index');
    }


    /***
     *Exibir todas as solcitações enviadas e aprovadas pelo chefe imediato, e que ainda não foram analisadas
     pelo RH OU que foram analisadas até a data corrente
     */
    public function resultAvalRHFerias(){

        $funcionario = Funcionario::orderBy('nome')
                ->join('lotacoes', 'funcionarios.lotacao_atual_id', 'lotacoes.id')

                ->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                    ->from('ferias_solicitacao')
                    ->whereColumn('ferias_solicitacao.funcionario_id', 'funcionarios.id')
                    ->where('ferias_solicitacao.status_id', '3'); //aprovado pelo Chefe imediato
                })
                ->orWhereExists(function ($query) {
                    $query->select(DB::raw(1))
                    ->from('ferias_solicitacao')
                    ->join('ferias_avaliacao', 'ferias_avaliacao.solicitacao_id', 'ferias_solicitacao.id')
                    ->whereColumn('ferias_solicitacao.funcionario_id', 'funcionarios.id')
                    ->where('ferias_avaliacao.tipo_avaliador', '2') //avaliação feita pelo RH
                    ->where(DB::raw('DATE_FORMAT(ferias_avaliacao.created_at, "%d/%m/%Y")'),  date('d/m/Y'));
                })

                ->select([
                    'funcionarios.id','funcionarios.matricula', 'funcionarios.nome', 'lotacoes.descricao as lotacao',
                     DB::raw('(SELECT COUNT(*) FROM ferias_solicitacao
                          WHERE ferias_solicitacao.funcionario_id = funcionarios.id
                          and ferias_solicitacao.status_id = 3
                          ) as qtde_sol'),

                          DB::raw('(SELECT
                          DATE_FORMAT(MIN(ferias_solicitacao.data_inicial), "%d/%m/%Y")
                          FROM ferias_solicitacao
                          WHERE ferias_solicitacao.funcionario_id = funcionarios.id
                          and ferias_solicitacao.status_id = 3
                          ) as dtinicio_proxima')
                 ])
                 ->get();

        return DataTables::of($funcionario)->make(true);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FeriasAvaliacaoRequest $request)
    {

        //GEt funcionario vinculado a usuário logado
        $user = new User();
        $avaliador = $user->getFuncionarioUserLogado();



        //Se foi indeferido e não informou motivo
        if(!$request->status && !$request->motivo_indeferido){
            rq::session()->flash('status-not', 'O campo Motivo Indeferimento é obrigatório.');
            return redirect()->back();
        }


        //se for deferido, campos devem estar preenchidos
        if($request->status && (!$request->ano || !$request->data_expedicao ||
          !$request->ano_aquisitivo_inicial || !$request->ano_aquisitivo_final || !$request->data_retorno)){
            rq::session()->flash('status-not', 'Os campos obrigatórios devem estar preenchidos.');
            return redirect()->back();
        }


        $solicitacao = FeriasSolicitacao::find($request->solicitacao_id);
        $statusSolicitacao = FeriasAvaliacaoRHController::getStatusSolicitacao($request->status);

        DB::beginTransaction();
        try{
            $avaliacao = FeriasAvaliacao::create([

                'solicitacao_id' => $request->solicitacao_id,
                'avaliador_id' => $avaliador->id,
                'status'  => $request->status,
                'tipo_avaliador'  => '2',
                'motivo_indeferido'  => $request->motivo_indeferido,
                'user_id' => auth()->user()->id,

            ]);


            //atualizar o status da solicitação
            $saveStatus = $solicitacao->update([
                'status_id'=> $statusSolicitacao,

            ]);

            //se solicitacao foi deferida pela RH, salvar Férias
            if($request->status){

                $ferias = new Ferias();
                $salvarFerias = $ferias->salvarFerias($request, $solicitacao);

            }


            if($avaliacao && $saveStatus && (!$request->status || ($request->status && $salvarFerias ))){
                DB::commit();
                rq::session()->flash('status', 'Avaliação de Férias salva com sucesso.');
            }else{
                DB::rollBack();
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar avaliação de férias.');
            }

            return redirect()->action('FeriasAvaliacaoRHController@show', $solicitacao->funcionario_id);
            //return redirect()->action('FeriasAvaliacaoRHController@index');

        }catch (\Exception $e){
            DB::rollBack();
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar avaliação de férias.'.$e->getMessage());
            return redirect()->action('FeriasAvaliacaoRHController@index');
        }


    }




    public static function getStatusSolicitacao($statusAvalSuperior){

        if($statusAvalSuperior){

            return 5; //Deferido pelo RH

        }else{

            return 6; //Indeferido pelo RH
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\FeriasSolicitacao  $feriasSolicitacao
     * @return \Illuminate\Http\Response
     */

    //ver a tela com as solicitacoes em aberto
    public function show($idFuncionario)
    {
            $funcionario = Funcionario::find($idFuncionario);

            //enviadas p análise do rh
            $solicitacoes = FeriasSolicitacao::where('funcionario_id', $idFuncionario)
                            ->where('status_id', '3')
            ->get();

            //já analisadas pelo rh
            $solicitacoesrh = FeriasSolicitacao::where('funcionario_id', $idFuncionario)
                ->join('ferias_avaliacao', 'ferias_avaliacao.solicitacao_id', 'ferias_solicitacao.id')
                ->where('ferias_avaliacao.tipo_avaliador', '2')
                ->select('ferias_solicitacao.*', 'ferias_avaliacao.status', 'ferias_avaliacao.avaliador_id')
                ->get();


            return view('ferias.aval_rh.solicitacoes', compact('funcionario', 'solicitacoes',  'solicitacoesrh'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FeriasSolicitacao  $feriasSolicitacao
     * @return \Illuminate\Http\Response
     */
   // public function edit(FeriasSolicitacao $feriasSolicitacao
    public function edit($id)
    {

        $solicitacao = FeriasSolicitacao::find($id);

        $ferias = new Ferias();
        $ferias->ano = date('Y');
        $ferias->data_expedicao = date('d/m/Y');

        return view('ferias.aval_rh.formulario', compact('solicitacao', 'ferias'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FeriasSolicitacao  $feriasSolicitacao
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FeriasSolicitacao $feriasSolicitacao)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FeriasSolicitacao  $feriasSolicitacao
     * @return \Illuminate\Http\Response
     */
    public function destroy(FeriasSolicitacao $feriasSolicitacao)
    {
        //
    }
}
