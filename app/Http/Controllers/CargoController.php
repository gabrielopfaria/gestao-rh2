<?php

namespace App\Http\Controllers;

use App\Cargo;
use App\Http\Requests\StoreCargo;
use App\Http\Requests\UpdateCargo;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CargoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cargos = Cargo::all();
        return view('cargos.index',compact('cargos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cargos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCargo $request)
    {
        DB::beginTransaction();
        $validado = $request->validated();
        $cargo = Cargo::create([
            'codigo'=>$validado['codigo'],
            'descricao'=>$validado['descricao'],
            'usuario_id_fk'=>auth()->user()->id
        ]);
        if(!$cargo){
            DB::rollBack();
            return redirect()->route('cargo.index')->with('error',"Não foi possível criar o cargo");
        }
        DB::commit();
        return redirect()->route('cargo.index')->with('status',"Cargo criado com sucesso");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cargo  $cargo
     * @return \Illuminate\Http\Response
     */
    public function show(Cargo $cargo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cargo  $cargo
     * @return \Illuminate\Http\Response
     */
    public function edit(Cargo $cargo)
    {
        // dd($cargo);
        return view('cargos.edit',compact('cargo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cargo  $cargo
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCargo $request, Cargo $cargo)
    {
        DB::beginTransaction();
        try{
            $cargo->update($request->validated());
        }catch(Exception $e){
            DB::rollBack();
            return redirect()->route('cargo.index')->with('error',"Não foi possível alterar o cargo");
        }
        DB::commit();
        return redirect()->route('cargo.index')->with('status',"Cargo alterado com sucesso");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cargo  $cargo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cargo $cargo)
    {
        //
    }
}
