<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTipoPagamento;
use App\TipoPagamento;
use Illuminate\Http\Request;
use Request as rq;

class TipoPagamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipopagamento = TipoPagamento::orderBy('descricao')->get();

        return view('tipopagamento.index', compact('tipopagamento'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipopagamento = new TipoPagamento();

        return view('tipopagamento.formulario', compact('tipopagamento'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTipoPagamento $request)
    {
        $tipopagamento = new TipoPagamento();
        $existeCodigo = $tipopagamento->existeCodigocadastrado($request->codigo, NULL);

        if($existeCodigo){
            rq::session()->flash('status-not', 'Já existe um tipo de pagamento cadastrado com o código informado.');
            return redirect()->back()->withInput();
        }

        try{

            $tipopagamento = TipoPagamento::create([
                'codigo' => $request->codigo,
                'descricao' => $request->descricao,
                'usuario_id_fk' => auth()->user()->id
            ]);

            if($tipopagamento){
                rq::session()->flash('status', 'Tipo de pagamento cadastrado com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar tipo de pagamento.');
            }

        }catch(\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar tipo de pagamento. '.$e->getMessage());
        }

        return redirect()->route('tipopagamento.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoPagamento  $tipoPagamento
     * @return \Illuminate\Http\Response
     */
    public function show(TipoPagamento $tipopagamento)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TipoPagamento  $tipoPagamento
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoPagamento $tipopagamento)
    {
        return view('tipopagamento.formulario', compact('tipopagamento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TipoPagamento  $tipoPagamento
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTipoPagamento $request, TipoPagamento $tipopagamento)
    {

        $existeCodigo = $tipopagamento->existeCodigocadastrado($request->codigo, $tipopagamento->id);

        if($existeCodigo){
            rq::session()->flash('status-not', 'Já existe um tipo de pagamento cadastrado com o código informado.');
            return redirect()->back()->withInput();
        }

        try{

            $tipopagamento = $tipopagamento->update([
                'codigo' => $request->codigo,
                'descricao' => $request->descricao,
            ]);

            if($tipopagamento){
                rq::session()->flash('status', 'Tipo de pagamento atualizado com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar tipo de pagamento.');
            }

        }catch(\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar tipo de pagamento. '.$e->getMessage());
        }

        return redirect()->route('tipopagamento.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoPagamento  $tipoPagamento
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoPagamento $tipoPagamento)
    {
        //
    }

    public function delete($id)
    {
        $tipopagamento = TipoPagamento::findOrFail($id);

        try{
            $tipopagamento->delete();
            rq::session()->flash('status', 'Tipo de pagamento '.$tipopagamento->descricao.' excluído com sucesso.');
            return redirect()->route('tipopagamento.index');
        }
        catch (\Exception $e){

            if($e->getCode() == 23000 ){
                rq::session()->flash('status-not', 'Tipo de pagamento não foi excluído. O mesmo está sendo utilizado em outro cadastro! ');
            }else{
                rq::session()->flash('status-not', 'Tipo de pagamento não foi excluído! '.$e->getMessage());
            }

            return redirect()->route('tipopagamento.index');
        }

    }
}
