<?php

namespace App\Http\Controllers;

use App\Cargo;
use App\CargoGenerico;
use App\Funcao;
use App\Funcionario;
use App\HorarioTrabalho;
use App\Http\Requests\StoreFuncionario;
use App\Lotacao;
use App\LotacaoGenerico;
use App\Naturalidade;
use App\Situacao;
use App\Turno;
use App\Utils\StringsUtil;
use App\Vinculo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Request as rq;

class FuncionarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $b)
    {
        if (isset($b->busca) && $b->busca != "") {
            $busca = $b->busca;
            $funcionarios = Funcionario::where('nome', 'like', '%' . $busca . '%')
                ->orWhere('cpf', 'like', '%' . $busca . '%')
                ->orWhere('matricula', 'like', '%' . $busca . '%')->get();
            if (count($funcionarios) > 0) {
                return view('funcionario.index', compact('funcionarios', 'busca'));
            } else {
                rq::session()->flash('status-not', 'Funcionário não encontrado');
                return redirect()->route('home');
            }
        } /* else {
            rq::session()->flash('status-not', 'Campo Vazio.');
        } */
        return redirect()->route('home');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lotacao_prodam = Lotacao::orderBy('descricao')->get();
        $funcao = Funcao::orderBy('descricao')->get();
        $lotacao = LotacaoGenerico::orderBy('descricao')->get();
        $horario_trabalho = HorarioTrabalho::all();
        $cargo_prodam = Cargo::orderBy('descricao')->get();
        $cargo = CargoGenerico::all();
        $turno = Turno::all();
        $vinculo = Vinculo::all();
        $situacao = DB::table('situacoes')->get();
        $naturalidade = Naturalidade::orderBy('descricao')->get();

        return view('funcionario.create', compact(
            'lotacao_prodam',
            'funcao',
            'lotacao',
            'horario_trabalho',
            'cargo_prodam',
            'cargo',
            'turno',
            'vinculo',
            'situacao',
            'naturalidade',
        ));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFuncionario $request)
    {
        try {
            $data = $request->all();
            $data['matricula'] = StringsUtil::limpaMatricula($request->matricula);
            $data['cpf'] = StringsUtil::limpaCPF_CNPJ($request->cpf);
            // dd($data);

            Funcionario::create($data);

            rq::session()->flash('status', 'Funcionário cadastrado com sucesso!');
            return redirect()->route('home');
        } catch (\Exception $e) {
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar funcionario. ' . $e->getMessage());
            return redirect()->route('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Funcionario  $funcionario
     * @return \Illuminate\Http\Response
     */
    public function show(Funcionario $funcionario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Funcionario  $funcionario
     * @return \Illuminate\Http\Response
     */
    public function edit(Funcionario $funcionario)
    {
        $situacao = Situacao::all();
        $naturalidade = DB::table('naturalidades')->get();
        $lotacao = LotacaoGenerico::orderBy('descricao')->get();
        $lotacao_prodam = Lotacao::orderBy('descricao')->get();
        $vinculo = Vinculo::orderBy('descricao')->get();
        $horario_trabalho = HorarioTrabalho::orderBy('descricao')->get();
        $turno = Turno::orderBy('descricao')->get();
        $cargo_prodam = Cargo::all();
        $cargo = CargoGenerico::all();
        $funcao = Funcao::all();
        return view('funcionario.edit', compact(
            'funcionario',
            'situacao',
            'naturalidade',
            'lotacao',
            'funcao',
            'lotacao_prodam',
            'vinculo',
            'horario_trabalho',
            'turno',
            'cargo_prodam',
            'cargo',
        ));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Funcionario  $funcionario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Funcionario $funcionario)
    {
        try {
            $data = $request->all();
            $data['cpf'] = StringsUtil::limpaCPF_CNPJ($request->cpf);
            $data['matricula'] = StringsUtil::limpaMatricula($request->matricula);
            $funcionario->fill($data);

            if ($funcionario) {
                rq::session()->flash('status', 'Funcionário atualizado com sucesso.');
            } else {
                rq::session()->flash('status-not', 'Ocorreu um erro ao editar funcionario.');
            }
        } catch (\Exception $e) {
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar funcionario. ' . $e->getMessage());
            return redirect()->route('funcionario.index');
        }

        $funcionario->save();
        return redirect()->route('funcionario.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Funcionario  $funcionario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Funcionario $funcionario)
    {
        //
    }
}
