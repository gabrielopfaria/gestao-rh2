<?php

namespace App\Http\Controllers;

use App\FeriasSolicitacao;
use App\FeriasAvaliacao;
use App\Funcionario;
use App\Http\Requests\FeriasAvaliacaoRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Request as rq;

class FeriasAvaliacaoSuperiorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //lista de ferias para avaliação
    public function index()
    {
        $user_cpf= Auth::user()->cpf;

        $chefeimediato = new User();
        $chefeimediato = $chefeimediato->getFuncionarioUser($user_cpf);


      /*  $solicitacoes = FeriasSolicitacao::where('ferias_solicitacao.superior_id', $chefeimediato->id)
            ->leftJoin('ferias_avaliacao', 'ferias_avaliacao.solicitacao_id','ferias_solicitacao.id' )
            ->select('ferias_solicitacao.*')
            ->where('ferias_solicitacao.status_id', 2)
            ->whereNull('ferias_avaliacao.id')
            ->orderBy('ferias_solicitacao.created_at')->get();*/

        //só vê as solicitações dos funcionários que é chefe imediato e com status enviado para aprovação
        $solicitacoes = FeriasSolicitacao::where('ferias_solicitacao.superior_id', $chefeimediato->id)
            ->where('ferias_solicitacao.status_id', 2)
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                ->from('ferias_avaliacao')
                ->whereColumn('ferias_avaliacao.solicitacao_id', 'ferias_solicitacao.id')
                ->where('ferias_avaliacao.tipo_avaliador', '1');
            })
            ->orderBy('ferias_solicitacao.created_at')
            ->get();


        return view('ferias.aval_superior.index', compact('solicitacoes', 'chefeimediato'));
    }

    //lista de ferias avaliadas
    public function index_avaliados()
    {
        $user_cpf= Auth::user()->cpf;

        $chefeimediato = new User();
        $chefeimediato = $chefeimediato->getFuncionarioUser($user_cpf);

        //só vê as solicitações dos funcionários que é chefe imediato e com status enviado para aprovação
        $solicitacoes = FeriasSolicitacao::where('ferias_solicitacao.superior_id', $chefeimediato->id)
            ->join('ferias_avaliacao', function($join)
            {
                $join->on('ferias_avaliacao.solicitacao_id', '=', 'ferias_solicitacao.id')
                    ->where('ferias_avaliacao.tipo_avaliador', '=', '1');
            })
            ->select('ferias_solicitacao.*', 'ferias_avaliacao.status')
            ->orderBy('ferias_solicitacao.created_at')->get();

        return view('ferias.aval_superior.index_avaliados', compact('solicitacoes', 'chefeimediato'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FeriasAvaliacaoRequest $request)
    {

        //GEt funcionario vinculado a usuário logado
        $user = new User();
        $avaliador = $user->getFuncionarioUserLogado();

        //Se foi indeferido e não informou motivo
        if(!$request->status && !$request->motivo_indeferido){
            rq::session()->flash('status-not', 'O campo Motivo Indeferimento é obrigatório.');
            return redirect()->back();
        }

        $solicitacao = FeriasSolicitacao::find($request->solicitacao_id);
        $statusSolicitacao = FeriasAvaliacaoSuperiorController::getStatusSolicitacao($request->status);

        DB::beginTransaction();
        try{
            $avaliacao = FeriasAvaliacao::create([

                'solicitacao_id' => $request->solicitacao_id,
                'avaliador_id' => $avaliador->id,
                'status'  => $request->status,
                'tipo_avaliador'  => '1',
                'motivo_indeferido'  => $request->motivo_indeferido,
                'user_id' => auth()->user()->id,

            ]);


            //atualizar o status da solicitação
            $saveStatus = $solicitacao->update([
                'status_id'=> $statusSolicitacao,

            ]);


            if($avaliacao && $saveStatus){
                DB::commit();
                rq::session()->flash('status', 'Avaliação de Férias salva com sucesso.');
            }else{
                DB::rollBack();
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar avaliação de férias.');
            }

            return redirect()->action('FeriasAvaliacaoSuperiorController@index');

        }catch (\Exception $e){
            DB::rollBack();
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar avaliação de férias.'.$e->getMessage());
            return redirect()->action('FeriasAvaliacaoSuperiorController@index');
        }

    }

    public static function getStatusSolicitacao($statusAvalSuperior){

        if($statusAvalSuperior){

            return 3; //Deferido pelo chefe imediato

        }else{

            return 4; //Indeferido pelo chefe imediato.
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\FeriasSolicitacao  $feriasSolicitacao
     * @return \Illuminate\Http\Response
     */
    public function show(FeriasSolicitacao $feriasSolicitacao)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FeriasSolicitacao  $feriasSolicitacao
     * @return \Illuminate\Http\Response
     */
   // public function edit(FeriasSolicitacao $feriasSolicitacao
    public function edit($id)
    {
        $solicitacao = FeriasSolicitacao::find($id);

        return view('ferias.aval_superior.formulario', compact('solicitacao'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FeriasSolicitacao  $feriasSolicitacao
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FeriasSolicitacao $feriasSolicitacao)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FeriasSolicitacao  $feriasSolicitacao
     * @return \Illuminate\Http\Response
     */
    public function destroy(FeriasSolicitacao $feriasSolicitacao)
    {
        //
    }
}
