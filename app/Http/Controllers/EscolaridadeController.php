<?php

namespace App\Http\Controllers;

use App\Escolaridade;
use App\Http\Requests\StoreEscolaridade;
use Illuminate\Http\Request;
use Request as rq;

class EscolaridadeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $escolaridades = Escolaridade::orderBy('descricao')->withTrashed()->get();

        return view('escolaridade.index', compact('escolaridades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $escolaridade = new Escolaridade();

        return view('escolaridade.formulario', compact('escolaridade'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEscolaridade $request)
    {

        try{

            $escolaridade = Escolaridade::create([
                'descricao' => $request->descricao,
                'usuario_id_fk' => auth()->user()->id
            ]);

            if($escolaridade){
                rq::session()->flash('status', 'Escolaridade cadastrada com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar escolaridade.');
            }

        }catch(\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar escolaridade. '.$e->getMessage());
        }

        return redirect()->route('escolaridade.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Escolaridade  $escolaridade
     * @return \Illuminate\Http\Response
     */
    public function show(Escolaridade $escolaridade)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Escolaridade  $escolaridade
     * @return \Illuminate\Http\Response
     */
    public function edit(Escolaridade $escolaridade)
    {
        return view('escolaridade.formulario', compact('escolaridade'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Escolaridade  $escolaridade
     * @return \Illuminate\Http\Response
     */
    public function update(StoreEscolaridade $request, Escolaridade $escolaridade)
    {

        try{

            $escolaridade = $escolaridade->update([
                'descricao' => $request->descricao,
            ]);

            if($escolaridade){
                rq::session()->flash('status', 'Escolaridade atualizada com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar escolaridade.');
            }

        }catch(\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar escolaridade. '.$e->getMessage());
        }

        return redirect()->route('escolaridade.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Escolaridade  $escolaridade
     * @return \Illuminate\Http\Response
     */
    public function destroy(Escolaridade $escolaridade)
    {
        //
    }

    //para ativar apos soft delete
    public function ativar($id)
    {
        // Recupera pelo ID
        $escolaridade = Escolaridade::where('id', $id)->withTrashed()->first();

        // Restaura:
        $rest = $escolaridade->restore();

        if($rest){
            rq::session()->flash('status', 'Escolaridade ativada.');
        }else{
            rq::session()->flash('status-not', 'Ocorreu um erro ao ativar escolaridade.');
        }

        return redirect()->route('escolaridade.index');

    }

    //inativa com o soft delete
    public function delete($id)
    {
        $escolaridade = Escolaridade::findOrFail($id);

        try{
            $escolaridade->delete();
            rq::session()->flash('status', 'Escolaridade '.$escolaridade->descricao.' inativada com sucesso.');
            return redirect()->route('escolaridade.index');
        }
        catch (\Exception $e){

         /*
            if($e->getCode() == 23000 ){
                rq::session()->flash('status-not', 'Escolaridade não foi excluída. A mesma está sendo utilizada em outro cadastro! ');
            }else{
                rq::session()->flash('status-not', 'Escolaridade não foi excluída! '.$e->getMessage());
            }
          */

            rq::session()->flash('status-not', 'Escolaridade não foi inativada! '.$e->getMessage());

            return redirect()->route('escolaridade.index');
        }

    }
}
