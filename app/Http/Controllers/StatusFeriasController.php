<?php

namespace App\Http\Controllers;

use App\StatusFerias;
use Illuminate\Http\Request;

class StatusFeriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StatusFerias  $statusFerias
     * @return \Illuminate\Http\Response
     */
    public function show(StatusFerias $statusFerias)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StatusFerias  $statusFerias
     * @return \Illuminate\Http\Response
     */
    public function edit(StatusFerias $statusFerias)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StatusFerias  $statusFerias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StatusFerias $statusFerias)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StatusFerias  $statusFerias
     * @return \Illuminate\Http\Response
     */
    public function destroy(StatusFerias $statusFerias)
    {
        //
    }
}
