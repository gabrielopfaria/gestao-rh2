<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLotacao;
use App\Lotacao;
use Illuminate\Http\Request;
use Request as rq;

class LotacaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lotacoes = Lotacao::orderBy('descricao')->withTrashed()->get();

        return view('lotacao.index', compact('lotacoes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lotacao = new Lotacao();

        return view('lotacao.formulario', compact('lotacao'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLotacao $request)
    {
        $lotacao = new Lotacao();
        $existeCodigo = $lotacao->existeCodigocadastrado($request->codigo, NULL);

        if($existeCodigo){
            rq::session()->flash('status-not', 'Já existe uma lotação cadastrada com o código informado.');
            return redirect()->back()->withInput();
        }

        try{

            $lotacao = Lotacao::create([
                'codigo' => $request->codigo,
                'descricao' => $request->descricao,
                'usuario_id_fk' => auth()->user()->id
            ]);

            if($lotacao){
                rq::session()->flash('status', 'Lotação cadastrada com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar lotação.');
            }

        }catch(\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar lotação. '.$e->getMessage());
        }

        return redirect()->route('lotacao.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lotacao  $lotacao
     * @return \Illuminate\Http\Response
     */
    public function show(Lotacao $lotacao)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lotacao  $lotacao
     * @return \Illuminate\Http\Response
     */
    public function edit(Lotacao $lotacao)
    {
        return view('lotacao.formulario', compact('lotacao'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lotacao  $lotacao
     * @return \Illuminate\Http\Response
     */
    public function update(StoreLotacao $request, Lotacao $lotacao)
    {
        $existeCodigo = $lotacao->existeCodigocadastrado($request->codigo, $lotacao->id);

        if($existeCodigo){
            rq::session()->flash('status-not', 'Já existe uma lotação cadastrada com o código informado.');
            return redirect()->back()->withInput();
        }

        try{

            $lotacao = $lotacao->update([
                'codigo' => $request->codigo,
                'descricao' => $request->descricao,
            ]);

            if($lotacao){
                rq::session()->flash('status', 'Lotação atualizada com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar lotação.');
            }

        }catch(\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar lotação. '.$e->getMessage());
        }

        return redirect()->route('lotacao.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lotacao  $lotacao
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lotacao $lotacao)
    {
        //
    }

    //para ativar apos soft delete
    public function ativar($id)
    {
        // Recupera pelo ID
        $lotacao = Lotacao::where('id', $id)->withTrashed()->first();

        // Restaura:
        $rest = $lotacao->restore();

        if($rest){
            rq::session()->flash('status', 'Lotação ativada.');
        }else{
            rq::session()->flash('status-not', 'Ocorreu um erro ao ativar lotação.');
        }

        return redirect()->route('lotacao.index');

    }

    //inativar sof delete
    public function delete($id)
    {
        $lotacao = Lotacao::findOrFail($id);

        try{
            $lotacao->delete();
            rq::session()->flash('status', 'Lotação '.$lotacao->descricao.' inativada com sucesso.');
            return redirect()->route('lotacao.index');
        }
        catch (\Exception $e){

/*
            if($e->getCode() == 23000 ){
                rq::session()->flash('status-not', 'Lotação não foi excluída. A mesma está sendo utilizada em outro cadastro! ');
            }else{
                rq::session()->flash('status-not', 'Lotação não foi excluída! '.$e->getMessage());
            }
*/
            rq::session()->flash('status-not', 'Lotação não foi inativada! '.$e->getMessage());

            return redirect()->route('lotacao.index');
        }

    }
}
