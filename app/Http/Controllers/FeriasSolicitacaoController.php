<?php

namespace App\Http\Controllers;

use App\FeriasSolicitacao;
use App\FeriasAvaliacao;
use App\Funcionario;
use App\Http\Requests\FeriasSolicitacaoRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Utils\DateUtil;
use Illuminate\Support\Facades\DB;
use Request as rq;

class FeriasSolicitacaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;

        //usuario logado pode ver apenas as férias que solicitou
        $solicitacoes = FeriasSolicitacao::where('user_id', $user_id)
            ->orderByDesc('ferias_solicitacao.created_at')->get();


        $user_cpf = Auth::user()->cpf; //usuario possui cpf cadastrado
        $funcionario = Funcionario::where('cpf',  $user_cpf)->orderByDesc('id')->first(); //funcionario vinculado a usuario logado

        $equipe = Funcionario::where('lotacao_generico_id', $funcionario->lotacao_generico_id)->where('nome', '!=', $funcionario->nome)->orderBy('nome')->get();

        return view('ferias.solicitacao.index', compact('solicitacoes', 'user_cpf', 'funcionario', 'equipe'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $equipe = $request->equipe_id;

        $user_cpf = Auth::user()->cpf;

        if (!$user_cpf) {
            rq::session()->flash('status-not', 'Funcionário não vinculado a usuário. Contacte o administrador do sistema.');
            return redirect()->back();
        }

        $funcionario = Funcionario::where('cpf',  $user_cpf)->orderByDesc('id')->first();

        $solicitado = Funcionario::where('id',  $equipe)->orderByDesc('nome')->first();


        if (!$funcionario) {
            rq::session()->flash('status-not', 'O usuário logado não está associado a um funcionário. Contacte o administrador do sistema.');
            return redirect()->back();
        }

        $solicitacao = new FeriasSolicitacao();

        return view('ferias.solicitacao.formulario', compact('funcionario', 'solicitacao', 'solicitado'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FeriasSolicitacaoRequest $request)
    {

        $status = FeriasSolicitacao::STATUS_AGUARDAENVIO;

        try {
            $ferias = FeriasSolicitacao::create([

                'funcionario_id' => $request->solicitado_id,
                'superior_id' => $request->superior_id,
                'status_id'  => $status,
                'observacao'  => $request->observacao,
                'data_inicial'  => DateUtil::getDateDatabaseFromDateForm($request->data_inicial),
                'data_final'  => DateUtil::getDateDatabaseFromDateForm($request->data_final),
                'user_id' => auth()->user()->id,
            ]);

            if ($ferias) {
                rq::session()->flash('status', 'Solicitação de Férias cadastrada com sucesso.');
            } else {
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar solicitação de férias.');
            }

            return redirect()->action('FeriasSolicitacaoController@index');
        } catch (\Exception $e) {
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar solicitação de férias.' . $e->getMessage());
            return redirect()->action('FeriasSolicitacaoController@index');
        }
    }

    public function enviarAprovacaoSuperior(Request $request)
    {

        $solicitacao_id = $request->solicitacao_id;

        $solicitacao = FeriasSolicitacao::findOrFail($solicitacao_id);

        $status_id = FeriasSolicitacao::STATUS_ENVIOPARARH;

        try {
            $updateStatusFerias = $solicitacao->atualizarStatusSolFerias($solicitacao_id,  $status_id);


            if ($updateStatusFerias) {
                rq::session()->flash('status', 'Solicitação de Férias enviada para aprovação.');
            } else {
                rq::session()->flash('status-not', 'Ocorreu um erro ao enviar solicitação.');
            }

            return redirect()->action('FeriasSolicitacaoController@index');
        } catch (\Exception $e) {
            rq::session()->flash('status-not', 'Ocorreu um erro ao enviar solicitação de férias.' . $e->getMessage());
            return redirect()->action('FeriasSolicitacaoController@index');
        }
        /*

        $funcionario = Funcionario::findOrFail($solicitacao->funcionario_id);


        //nao está vinculado a um chefe imediato atualmente
        if (!$funcionario->superior_id) {
            rq::session()->flash('status-not', 'Funcionário não está vinculado a um chefe imediato atualmente.');
            return redirect()->back();
        } else {
            $updateChefe = $solicitacao->atualizarAvaliadorFerias($solicitacao_id,  $funcionario->superior_id);

            if (!$updateChefe) {
                rq::session()->flash('status-not', 'Ocorreu um erro no envio para aprovação. (#REF100)');
                return redirect()->back();
            }
        }

*/

        //se o chefe imediato que atual é diferente do que estava no momento do cadastro
        /* if($solicitacao->superior_id != $funcionario->superior_id){
            $updateChefe = $solicitacao->atualizarAvaliadorFerias($solicitacao_id,  $funcionario->superior_id);
        }*/
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FeriasSolicitacao  $feriasSolicitacao
     * @return \Illuminate\Http\Response
     */
    public function show(FeriasSolicitacao $feriasSolicitacao)
    {
        //
    }

    public function visualizaAprovacoes($idSolicitacao)
    {
        $solicitacao = FeriasSolicitacao::find($idSolicitacao);
        return view('ferias.historico.visualizar_aprovacoes', compact('solicitacao'));
    }

    public function visualizaAprovacoesAvaliador($tipoAvaliador, $idSolicitacao)
    {
        $solicitacao = FeriasSolicitacao::find($idSolicitacao);
        $avaliacao = FeriasAvaliacao::where('solicitacao_id', $idSolicitacao)
            ->where('tipo_avaliador', $tipoAvaliador)->get(); //visão de chefe imediato ou rh

        return view('ferias.historico.visualizacao_avaliador', compact('solicitacao', 'avaliacao'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FeriasSolicitacao  $feriasSolicitacao
     * @return \Illuminate\Http\Response
     */
    public function edit($idferiasSolicitacao)
    {
        $feriasSolicitacao = FeriasSolicitacao::find($idferiasSolicitacao);

        $feriasSolicitacao->data_inicial  = DateUtil::getDateFormFromDateDatabase($feriasSolicitacao->data_inicial);
        $feriasSolicitacao->data_final  = DateUtil::getDateFormFromDateDatabase($feriasSolicitacao->data_final);

        $user_cpf = Auth::user()->cpf;
        $funcionario = Funcionario::where('cpf',  $user_cpf)->orderByDesc('id')->first();
        return view('ferias.solicitacao.edit', compact('feriasSolicitacao', 'funcionario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FeriasSolicitacao  $feriasSolicitacao
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idferiasSolicitacao)
    {
        $feriasSolicitacao = FeriasSolicitacao::find($idferiasSolicitacao);
        // $feriasSolicitacao = $request->all();

        $data = $request->all();
        $data['data_inicial'] = DateUtil::getDateDatabaseFromDateForm($request->data_inicial);
        $data['data_final'] = DateUtil::getDateDatabaseFromDateForm($request->data_final);
        // dd($data['data_final']);
        $feriasSolicitacao->fill($data);
        $feriasSolicitacao->save();


        return redirect()->action('FeriasSolicitacaoController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FeriasSolicitacao  $feriasSolicitacao
     * @return \Illuminate\Http\Response
     */
    public function destroy(FeriasSolicitacao $feriasSolicitacao)
    {
        //
    }
}
