<?php

namespace App\Http\Controllers;

use App\HorarioTrabalho;
use App\Http\Requests\StoreHorarioTrabalho;
use Illuminate\Http\Request;
use Request as rq;

class HorarioTrabalhoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $horariotrabalho = HorarioTrabalho::orderBy('descricao')->get();
        return view('horariotrabalho.index',compact('horariotrabalho'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $horariotrabalho = new HorarioTrabalho();

        return view('horariotrabalho.create', compact('horariotrabalho'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreHorarioTrabalho $request)
    {
        try{
            $horariotrabalho = HorarioTrabalho::create([
                'descricao' => $request->descricao,
                'usuario_id_fk' => auth()->user()->id
            ]);
            if($horariotrabalho){
                rq::session()->flash('status', 'Horário de trabalho cadastrado com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar horário de trabalho.');
            }
        }catch (\Exception $e){
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar horario de trabalho. '.$e->getMessage());
            }

            return redirect()->route('horariotrabalho.index');

}

    /**
     * Display the specified resource.
     *
     * @param  \App\HorarioTrabalho  $horarioTrabalho
     * @return \Illuminate\Http\Response
     */
    public function show(HorarioTrabalho $horarioTrabalho)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HorarioTrabalho  $horarioTrabalho
     * @return \Illuminate\Http\Response
     */
    public function edit(HorarioTrabalho $horariotrabalho)
    {
        return view ('horariotrabalho.edit',compact('horariotrabalho'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HorarioTrabalho  $horarioTrabalho
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HorarioTrabalho $horariotrabalho)
    {
        try{
            $horariotrabalho = $horariotrabalho->update([
                'descricao' => $request->descricao,
                'usuario_id_fk' => auth()->user()->id
            ]);
            if($horariotrabalho){
                rq::session()->flash('status', 'Horário atualizado com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar horário.');
            }

        }catch(\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar horário. '.$e->getMessage());
        }
        return redirect()->route('horariotrabalho.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HorarioTrabalho  $horarioTrabalho
     * @return \Illuminate\Http\Response
     */
    public function destroy(HorarioTrabalho $horarioTrabalho)
    {
        //
    }
}
