<?php

namespace App\Http\Controllers;

class BtnController extends Controller
{

    public static function delete($id, $reference=null, $function=null, $title=null){

        $question = 'Deseja excluir '. $function . ' ' . $reference . '?';
        return view('messages.deletar', compact('id', 'reference', 'title', 'question'))->render();
    }


}
