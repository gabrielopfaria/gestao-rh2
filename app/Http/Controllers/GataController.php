<?php

namespace App\Http\Controllers;

use App\Gata;
use Illuminate\Http\Request;

class GataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gata  $gata
     * @return \Illuminate\Http\Response
     */
    public function show(Gata $gata)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Gata  $gata
     * @return \Illuminate\Http\Response
     */
    public function edit(Gata $gata)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gata  $gata
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gata $gata)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gata  $gata
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gata $gata)
    {
        //
    }
}
