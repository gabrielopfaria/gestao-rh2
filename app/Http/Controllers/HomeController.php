<?php

namespace App\Http\Controllers;

use App\Funcionario;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $qtd = count(Funcionario::get('id'));

        // return view('home',compact('qtd'));
        return view('homesuperior');
    }
}
