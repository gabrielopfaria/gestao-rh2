<?php

namespace App\Http\Controllers\Api;

use App\Funcionario;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function teste(){
        return "ok";
    }
    public function getUserByCpf($cpf){
        return Funcionario::where('cpf',$cpf)->get();
    }
}
