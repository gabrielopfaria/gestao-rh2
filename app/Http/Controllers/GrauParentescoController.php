<?php

namespace App\Http\Controllers;

use App\GrauParentesco;
use App\Http\Requests\StoreGrauParentesco;
use Illuminate\Http\Request;
use Request as rq;

class GrauParentescoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grauparentesco = GrauParentesco::orderBy('descricao')->get();

        return view('grauparentesco.index', compact('grauparentesco'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $grauparentesco = new GrauParentesco();

        return view('grauparentesco.formulario', compact('grauparentesco'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGrauParentesco $request)
    {
        try{

            $grauparentesco = GrauParentesco::create([
                'descricao' => $request->descricao,
                'usuario_id_fk' => auth()->user()->id
            ]);

            if($grauparentesco){
                rq::session()->flash('status', 'Grau de parentesco cadastrado com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar grau de parentesco.');
            }

        }catch(\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar grau de parentesco. '.$e->getMessage());
        }

        return redirect()->route('grauparentesco.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GrauParentesco  $grauParentesco
     * @return \Illuminate\Http\Response
     */
    public function show(GrauParentesco $grauParentesco)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GrauParentesco  $grauParentesco
     * @return \Illuminate\Http\Response
     */
    public function edit(GrauParentesco $grauparentesco)
    {
        return view('grauparentesco.formulario', compact('grauparentesco'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GrauParentesco  $grauParentesco
     * @return \Illuminate\Http\Response
     */
    public function update(StoreGrauParentesco $request, GrauParentesco $grauparentesco)
    {
        try{

            $grauparentesco = $grauparentesco->update([
                'descricao' => $request->descricao,
            ]);

            if($grauparentesco){
                rq::session()->flash('status', 'Grau de parentesco atualizado com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar grau de parentesco.');
            }

        }catch(\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar grau de parentesco. '.$e->getMessage());
        }

        return redirect()->route('grauparentesco.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GrauParentesco  $grauParentesco
     * @return \Illuminate\Http\Response
     */
    //public function destroy(GrauParentesco $grauParentesco)
    public function delete($id)
    {

        $grauParentesco = GrauParentesco::findOrFail($id);

        try{
            $grauParentesco->delete();
            rq::session()->flash('status', 'Grau de parentesco '.$grauParentesco->descricao.' excluído com sucesso.');
            return redirect()->route('grauparentesco.index');
        }
        catch (\Exception $e){

            if($e->getCode() == 23000 ){
                rq::session()->flash('status-not', 'Grau de parentesco não foi excluído. O mesma está sendo utilizado em outro cadastro! ');
            }else{
                rq::session()->flash('status-not', 'Grau de parentesco não foi excluído! '.$e->getMessage());
            }

            return redirect()->route('grauparentesco.index');
        }
    }
}
