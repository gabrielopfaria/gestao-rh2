<?php

namespace App\Http\Controllers;

use App\Ferias;
use App\Funcionario;
use App\Http\Requests\FeriasRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Request as rq;
use App\Utils\DateUtil;

class FeriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ferias_old.index');
    }

    public function resultFuncionario(){

        $funcionario  = Funcionario::orderBy('nome')
                        ->select(DB::raw('DATE_FORMAT(data_admissao, "%d/%m/%Y") as dt_admissao,
                        concat("***.***.",substring(cpf, -5, 3),"-",substring(cpf, -2, 2)) cpf_format,
                        funcionarios.*'))
                        ->get();
        return DataTables::of($funcionario)->make(true);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(FeriasRequest $request)
    {
        try{
            $ferias = Ferias::create([
                'ano' => $request->ano,
                'data_expedicao' => DateUtil::getDateDatabaseFromDateForm($request->data_expedicao),
                'ano_aquisitivo_inicial' => $request->ano_aquisitivo_inicial,
                'ano_aquisitivo_final' => $request->ano_aquisitivo_final,
                'data_inicial' => DateUtil::getDateDatabaseFromDateForm($request->data_inicial),
                'data_final' => DateUtil::getDateDatabaseFromDateForm($request->data_final),
                'data_retorno' => DateUtil::getDateDatabaseFromDateForm($request->data_retorno),
                'qtd_dias' => $request->qtd_dias,
                'func_matricula' => $request->func_matricula,
                'funcionario_id' => $request->funcionario_id,
                'usuario_id_fk' =>auth()->user()->id,
            ]);

            if($ferias){
                rq::session()->flash('status', 'Férias cadastrada com sucesso.');
                return redirect()->action('FeriasController@show',$request->funcionario_id);
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar férias.');
                return redirect()->action('FeriasController@index');
            }

        }catch (\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar férias.'.$e->getMessage());
            return redirect()->action('FeriasController@index');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Ferias  $ferias
     * @return \Illuminate\Http\Response
     */
    //public function show(Ferias $ferias)
    public function show($idFunc)
    {
        $ferias = new Ferias();
        $ferias->ano = date('Y');
        $ferias->data_expedicao = date('d/m/Y');
        $funcionario = Funcionario::where('id',$idFunc)->first();

        $listagemFerias = Ferias::where('funcionario_id', $idFunc)->orderByDesc('ano')->get();

        return view('ferias_old.formulario', compact('funcionario', 'ferias', 'listagemFerias'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ferias  $ferias
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$ferias = Ferias::find($id);
        //dd($ferias );

        $funcionario = Funcionario::where('id',$id)->first();

        //$ferias = Ferias::where('funcionario_id')

        return view('ferias_old.formulario', compact('funcionario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ferias  $ferias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ferias $ferias)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ferias  $ferias
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ferias $ferias)
    {
        //
    }
}
