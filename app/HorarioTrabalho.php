<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HorarioTrabalho extends Model
{
    public $fillable =['descricao','usuario_id_fk'];
}
