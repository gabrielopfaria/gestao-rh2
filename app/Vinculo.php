<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vinculo extends Model
{
    public $fillable = ['sigla','descricao','usuario_id_fk'];
}
