<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Escolaridade extends Model
{
    protected $table = 'escolaridades';
    use SoftDeletes;

    public $fillable = ['descricao'];

}
