<?php

namespace App;

use App\Utils\DateUtil;
use App\Ferias;
use Illuminate\Database\Eloquent\Model;

class Ferias extends Model
{
    protected $table = 'ferias';
    public $timestamps = true;

    protected $fillable = [
        'ano','data_expedicao','ano_aquisitivo_inicial','ano_aquisitivo_final','user_id',
        'data_inicial','data_final','data_retorno','qtd_dias','func_matricula', 'funcionario_id', 'solicitacao_id'
    ];


    public function salvarFerias($request, $solicitacao){


        $data_expedicao = NULL;
        if($request->data_expedicao){
            $data_expedicao = DateUtil::getDateDatabaseFromDateForm($request->data_expedicao);
        }

        $data_retorno = NULL;
        if($request->data_retorno){
            $data_retorno =  DateUtil::getDateDatabaseFromDateForm($request->data_retorno);
        }



        $ferias = Ferias::create([

            'ano' => $request->ano,
            'data_expedicao' =>  $data_expedicao,
            'ano_aquisitivo_inicial' => $request->ano_aquisitivo_inicial,
            'ano_aquisitivo_final' => $request->ano_aquisitivo_final,

            'data_inicial' => $solicitacao->data_inicial,
            'data_final' => $solicitacao->data_final,
            'data_retorno' =>  $data_retorno,

            'qtd_dias' => DateUtil::qtdDias($solicitacao->data_inicial,  $solicitacao->data_final),

            'func_matricula' =>  $solicitacao->funcionario->matricula,
            'funcionario_id' => $solicitacao->funcionario_id,

            'solicitacao_id' => $solicitacao->id,
            'user_id' => auth()->user()->id,

        ]);

        return $ferias;

    }







}
