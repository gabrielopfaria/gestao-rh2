<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeriasAvaliacao extends Model
{
    protected $table = 'ferias_avaliacao';
    public $timestamps = true;

    protected $fillable = [
        'solicitacao_id',	'avaliador_id',	'status', 	'motivo_indeferido',
        'tipo_avaliador', 'user_id', 'created_at'
    ];

    public function avaliador(){
        return $this->belongsTo('App\Funcionario', 'avaliador_id', 'id');
    }

    /*
    public function solicitacao(){
        return $this->hasOne('App\FeriasSolicitacao', 'solicitacao_id');
    }*/

    public function gettipoAvaliador(){
        switch($this->tipo_avaliador){
            case 1: return "Chefe Imediato";break;
            case 2: return "RH";break;
            case NULL: return "";break;

        }
    }

    public function getavaliacao(){
        switch($this->status){
            case 0: return "Indeferido";break;
            case 1: return "Deferido";break;
            case NULL: return "";break;

        }
    }
}
