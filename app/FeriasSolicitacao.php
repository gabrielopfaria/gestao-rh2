<?php

namespace App;

use App\Funcionario;

use Illuminate\Database\Eloquent\Model;

class FeriasSolicitacao extends Model
{
    protected $table = 'ferias_solicitacao';
    public $timestamps = true;

    const STATUS_AGUARDAENVIO = 1;
    // const STATUS_ENVIOPARARH = 2;
    const STATUS_ENVIOPARARH = 3;

    protected $fillable = [
        'funcionario_id',	'status_id',	'observacao', 	'superior_id',
        'user_id', 'data_inicial', 'data_final'
    ];

    public function getreferencia(){
        switch($this->id){
            case $this->id: return "SOLF".$this->id ;break;
        }
    }

    public function status(){
        return $this->belongsTo('App\StatusFerias', 'status_id', 'id');
    }

    public function superior(){
        return $this->belongsTo('App\Funcionario', 'superior_id', 'id');
    }

    public function funcionario(){
        return $this->belongsTo('App\Funcionario', 'funcionario_id', 'id');
    }

    public function aprovacoes(){
        return $this->hasMany('App\FeriasAvaliacao', 'solicitacao_id', 'id');
    }

    //atualizar chefe imediato da solicitacao de ferias para o chefe imediato atual
    public static function atualizarAvaliadorFerias($idSolicitacao,$superior_id){

        $solicitacao = FeriasSolicitacao::find($idSolicitacao);
        $solicitacao->superior_id = $superior_id;

        try{
            $solicitacao->update();
            return true;
        }catch (Exception $e){
            return false;
        }
    }

    //atualizar status da solicitação de férias
    public static function atualizarStatusSolFerias($idSolicitacao,$status_id){

        $solicitacao = FeriasSolicitacao::find($idSolicitacao);
        $solicitacao->status_id = $status_id;

        try{
            $solicitacao->update();
            return true;
        }catch (Exception $e){
            return false;
        }
    }

    public function getavaliacao(){
        switch($this->status){
            case 0: return "Indeferido";break;
            case 1: return "Deferido";break;
            case NULL: return "";break;

        }
    }

    public function getColor($solicitacao_id){

        $resultado = FeriasSolicitacao::where('id', $solicitacao_id)
        ->select('status_id')->first();
        switch($resultado->status_id){
            case 3: return "style=color:green";break;
            case 4: return "style=color:red";break;
            case 5: return "style=color:green";break;
            case 6: return "style=color:red";break;
            case NULL: return "";break;

        }
        //return $resultado?"selected":"";
    }


    public function getAvaliador($func_id){

        $funcionario = Funcionario::where('id', $func_id)->first();

        return $funcionario;

    }


}
