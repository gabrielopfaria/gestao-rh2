<?php

namespace App;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class Banco extends Model
{
    protected $table = "bancos";
    public $fillable = ['codigo', 'descricao'];

    //verifica se já existe codigo cadastrado
   public function existeCodigocadastrado($codigo, $id){

    if($id){//update

        $existe = DB::table('bancos')->where([
            ['codigo', '=', $codigo],
            ['id', '<>', $id],
        ])->count();

    }else{//store
        $existe = DB::table('bancos')->where([
            ['codigo', '=', $codigo],
        ])->count();
    }

    if($existe > 0){
        return true;
    }else{
        return false;
    }

}
}
