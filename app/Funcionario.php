<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Funcionario extends Model
{
    protected $table = "funcionarios";
    // protected $appends = ['descricao'];
    public $fillable = [
        'matricula',
        'nome',
        'sexo',
        'data_nascimento',
        'naturalidade_id',
        'nacionalidade',
        'rg',
        'cpf',
        'data_admissao',
        'vinculo_id',
        'lotacao_atual_id',
        'lotacao_generico_id',
        'cargo_id',
        'cargo_generico_id',
        'funcao_id',
        'horario_trabalho_id',
        'turno_id',
        'situacao_id',
        'superior_id',
        'observacao',
    ];

    public function DataAdmissao()
    {
        return date('d/m/Y', strtotime($this->data_admissao));
    }

    public function lotacao()
    {
        return $this->belongsTo('App\LotacaoGenerico', 'lotacao_generico_id', 'id');
    }

    public function situacao()
    {
        return $this->belongsTo('App\Situacao', 'situacao_id', 'id');
    }

    public function superior()
    {
        return $this->belongsTo('App\Funcionario', 'superior_id', 'id');
    }
    public function getIdadeAttribute()
    {
        $data = Carbon::createFromFormat('Y-m-d', $this->attributes['data_nascimento']);

        return $data->diffInYears(Carbon::now());
    }
    public function getDescricaoAttribute()
    {
        return $this->lotacao->descricao;
    }
    public function buscarFuncionario()
    {
        return $this->lotacao;
    }
}
