<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrauParentesco extends Model
{
    protected $table = "grau_parentesco";
    public $fillable = ['descricao', 'usuario_id_fk'];
}
