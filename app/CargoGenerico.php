<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CargoGenerico extends Model
{
    protected $table = 'cargo_genericos';
    public $fillable =['cargo'];
}
