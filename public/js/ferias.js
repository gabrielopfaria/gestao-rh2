$(document).ready(function(){
    $('#data_inicial').on('change', function () {
        var txtVal =  $(this).val();
        if(!isDate(txtVal))
            $(this).val("");
    });

    $('#data_expedicao').on('change', function () {
        var txtVal =  $(this).val();
        if(!isDate(txtVal))
            $(this).val("");
    });

    $('#data_retorno').on('change', function () {
        var txtVal =  $(this).val();
        if(!isDate(txtVal))
            $(this).val("");
    });

});


function CalcularData() {
    if ($('#data_inicial').val() != "" && $('#qtd_dias').val() != "") {
        var qtd_dias = parseInt($('#qtd_dias').val());
        $('#data_final').val(somarDataComDias($('#data_inicial').val(), qtd_dias-1));
        $('#data_retorno').val(somarDataComDias($('#data_final').val(), 1));
    }
}

function CalcularDataSol() {

    qtdeDias = $('#qtd_dias').val();

    if(qtdeDias == "" || !qtdeDias || qtdeDias == 0){

        $('#data_final').val("");
    }

    else if ($('#data_inicial').val() != "" && qtdeDias != "") {
        var qtd_dias = parseInt($('#qtd_dias').val());
        $('#data_final').val(somarDataComDias($('#data_inicial').val(), qtd_dias-1));

    }
}

function SomarAno() {
    if ($('#ano_aquisitivo_inicial').val() != "") {
        var ano_inicial = parseInt($('#ano_aquisitivo_inicial').val());
        $('#ano_aquisitivo_final').val(ano_inicial + 1);
    }
}


