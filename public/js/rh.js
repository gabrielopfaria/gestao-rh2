$(document).ready(function(){

    $('.cpf').mask('000.000.000-00', {placeholder: "000.000.000-00", clearIfNotMatch: true});
    $('.matricula').mask('999.999-9S', {placeholder: "000.000-0A", clearIfNotMatch: true});
    $('.telefone').mask('(99) 99999-9999', {placeholder: "(00) 00000-0000", clearIfNotMatch: true});
    $('.codebanco').mask('0000000000', {clearIfNotMatch: false});
    $('.codetipopagamento').mask('0000000000', {clearIfNotMatch: false});
    $('.codelotacao').mask('9.999.999.999.999');
    $('.data').mask('99/99/9999', {placeholder: "00/00/0000"});
    $('.ano').mask('9999', {clearIfNotMatch: true});
    $('.qtd_dias').mask('99999');

});
function teste(){
    a=2;
    return a;
}

function somarDataComDias(data_inicial,qtd_dias) {

    var parts = data_inicial.split("/");
    var data = new Date(parts[2], parts[1] - 1, parts[0]);
    data.setDate(data.getDate() + parseInt(qtd_dias));
    var novaData = data.toLocaleDateString("pt-BR");

    return novaData;

}

//data válida
function isDate(txtDate)
{
    var currVal = txtDate;
    if(currVal == '')
        return false;

    //Declare Regex
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return false;

    //Checks for dd/mm/yyyy format.
    dtDay = dtArray[1];
    dtMonth= dtArray[3];
    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay> 31)
        return false;
    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
        return false;
    else if (dtMonth == 2)
    {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay> 29 || (dtDay ==29 && !isleap))
            return false;
    }
    return true;
}


function dataAtualFormatada(){
    var data = new Date(),
        dia  = data.getDate().toString(),
        diaF = (dia.length == 1) ? '0'+dia : dia,
        mes  = (data.getMonth()+1).toString(), //+1 pois no getMonth Janeiro começa com zero.
        mesF = (mes.length == 1) ? '0'+mes : mes,
        anoF = data.getFullYear();
    return diaF+"/"+mesF+"/"+anoF;
}

//calcular idade no cadastro de funcionário
function Idade() {

    var hoje = new Date();
    var datanascimento = $('#data_nascimento').val();
    var arrayData = datanascimento.split("/");

    var retorno = "";

    if (arrayData.length == 3) {
        // Decompoem a data em array
        var ano = parseInt(arrayData[2]);
        var mes = parseInt(arrayData[1]);
        var dia = parseInt(arrayData[0]);

        // Valida a data informada
        if (arrayData[0] > 31 || arrayData[1] > 12) {
            return retorno;
        }

        ano = (ano.length == 2) ? ano += 1900 : ano;

        // Subtrai os anos das duas datas
        var idade = (hoje.getYear() + 1900) - ano;

        // Subtrai os meses das duas datas
        var meses = (hoje.getMonth() + 1) - mes;

        // Se meses for menor que 0 entao nao cumpriu anos. Se for maior sim ja cumpriu
        idade = (meses < 0) ? idade - 1 : idade;

        meses = (meses < 0) ? meses + 12 : meses;

        retorno = (idade + " a " + meses + " m");
    }

    $('#idade').val(retorno);
}
