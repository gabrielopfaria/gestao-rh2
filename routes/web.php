<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Funcionario;
// use Illuminate\Routing\Route;

Auth::routes();
Route::middleware(['auth'])->group(function () {
    Route::redirect('/', '/home');
    // Route::any('/home', 'HomeController@index')->name('home');
    Route::any('/home', 'HomeController@index')->name('homesuperior');

    // Funcionários
    Route::resource('/funcionario', 'FuncionarioController');

    Route::get('/funcionario/novo', function () {
        return view('funcionario.formulario');
    });
    // Cargos
    Route::resource('/cargo', 'CargoController');

    // Vínculos
    Route::resource('/vinculo', 'VinculoController');

    // Escolaridade
    Route::resource('/escolaridade', 'EscolaridadeController');
    Route::get('/escolaridade/remove/{id}', 'EscolaridadeController@delete');
    Route::get('/escolaridade/ativar/{id}', 'EscolaridadeController@ativar');

    // Grau de Parentesco
    Route::resource('/grauparentesco', 'GrauParentescoController');
    Route::get('/grauparentesco/remove/{id}', 'GrauParentescoController@delete');

    // Banco
    Route::resource('/banco', 'BancoController');
    Route::get('/banco/remove/{id}', 'BancoController@delete');

    // Tipo de Pagamento
    Route::resource('/tipopagamento', 'TipoPagamentoController');
    Route::get('/tipopagamento/remove/{id}', 'TipoPagamentoController@delete');

    // Horario de Trabalho
    Route::resource('/horariotrabalho', 'HorarioTrabalhoController');

    // Lotação
    Route::resource('/lotacao', 'LotacaoController');
    Route::get('/lotacao/remove/{id}', 'LotacaoController@delete');
    Route::get('/lotacao/ativar/{id}', 'LotacaoController@ativar');

    // Férias
    //Route::resource('/ferias','FeriasController');
    //Route::get('/ferias/cadastro/{idFunc}', 'FeriasController@createFerias');
    //Route::post('/ferias/', 'FeriasController@store');
    //Route::get('/ferias/', 'FeriasController@index');
    // Route::get('/ferias/edit/{id}', 'FeriasController@edit');
    //Route::get('resultFuncionario', array('as'=>'resultFuncionario','uses'=>'FeriasController@resultFuncionario'));

    // Férias Solicitação
    Route::resource('/ferias/solicitacao', 'FeriasSolicitacaoController');
    Route::post('/ferias/solicitacao/enviarAprovacao', 'FeriasSolicitacaoController@enviarAprovacaoSuperior');

    //Alterar Data
    Route::get('/ferias/solicitacao/{feriasSolicitacao}/edit', 'FeriasSolicitacaoController@edit')->name('edit');

    //visualizar histórico de férias
    Route::get('visualiza/historico/{feriasSolicitacao}', 'FeriasSolicitacaoController@visualizaAprovacoes')->name('historico');
    //visao superior ou rh
    Route::get('visualiza/avaliacao/{tipoAvaliador}/{feriasSolicitacao}', 'FeriasSolicitacaoController@visualizaAprovacoesAvaliador')->name('historicoavaliador');

    // Férias Avaliação Chefe imediato
    Route::resource('/ferias/avaliacao/superior', 'FeriasAvaliacaoSuperiorController');
    Route::get('/ferias/avaliacao/lista', 'FeriasAvaliacaoSuperiorController@index_avaliados');


    //Férias Avaliação RH
    Route::resource('/ferias/avaliacao/rh', 'FeriasAvaliacaoRHController');
    Route::get('resultAvalRHFerias', array('as' => 'resultAvalRHFerias', 'uses' => 'FeriasAvaliacaoRHController@resultAvalRHFerias'));

    // Lotação
    Route::resource('/turno', 'TurnoController');
    Route::get('/turno/remove/{id}', 'TurnoController@delete');
    Route::get('/turno/ativar/{id}', 'TurnoController@ativar');
});
