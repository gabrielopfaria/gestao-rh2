@extends('layouts.pagina')

@section('content')
    <div class="col-12">
        @include('messages.alert')
        <div class="row" style="background-color: #ececec; border-radius: 10px">
            {{-- Ficha Funcional --}}
            <div class="col-sm-3 col-lg-3 text-center mt-3 mb-5">
                <h3>Solicitação de Férias</h3>

                <a href="{{url('ferias/solicitacao')}}">
                    <div class="btn btn-block btn-primary">
                        Solicitar Férias
                    </div>
                </a>


            </div>
        </div>
    @endsection
