@extends('layouts.pagina')

@section('content')

<div class="col-12">

@include('messages.alert')

@if($banco->exists)
    <div class="col-12 text-center">
        <h3>EDITAR BANCO</h3>
    </div>

    <form action="{{route('banco.update', $banco)}}" method="post">
        <input type="hidden" name="_method" value="put">

@else

    <div class="col-12 text-center">
        <h3>CADASTRO DE BANCO</h3>
    </div>

    <form action="{{route('banco.store')}}" method="post">
@endif
        {{ csrf_field() }}

            <div class="form-group col-3">
                <label>Código *</label>
                <input name="codigo" id="codigo" value="{{old('codigo',$banco->codigo)}}" type="text" class="form-control codebanco"  required />
            </div>

            <div class="form-group col-9">
                <label>Descrição *</label>
                <input name="descricao" id="descricao" value="{{old('descricao',$banco->descricao)}}" type="text" class="form-control" placeholder="Digite o nome do banco" required />
            </div>


        <div class="row">
            <div class="form-group form-footer col-12 text-center" style="margin-bottom: 50px">

                <button class="btn btn-primary" type="submit">Salvar</button>

                <a class="btn btn-default" href="{{route('banco.index')}}">
                    Cancelar
                </a>
            </div>
        </div>

        </form>

</div>
@endsection
