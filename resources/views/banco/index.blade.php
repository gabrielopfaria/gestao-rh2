@extends('layouts.pagina')

@section('content')

<div class="col-12">

@include('messages.alert')

        <div class="text-center col-12">
                <a href="{{route('banco.create')}}" class="btn btn-large btn-primary m-3">
                    <b>Novo Banco</b>
                </a>
            </div>

            <table class="table table-striped table-bordered" id="tbBanco">
                <thead>
                <tr class="bg-dark text-white text-center">
                    <th>Id</th>
                    <th>Código</th>
                    <th>Descrição</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($bancos as $b)
                <tr>
                    <td>{{$b->id}}</td>
                    <td>{{$b->codigo}}</td>
                    <td>{{$b->descricao}}</td>
                    <td>

                        <div class="dropdown open">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown">
                                        Selecione
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="triggerId">
                                        <a class="dropdown-item" href="{{action('BancoController@edit', $b)}}">Editar</a>
                                        <a class="dropdown-item"
                                        onclick="return confirm('Confirmar exclusão')"
                                        href="{{url('banco/remove/'.$b->id)}}">Excluir</a>
                                    </div>
                                </div>
                    </td>
                </tr>
                @endforeach

                </tbody>
            </table>

</div>

    <script>
        $(document).ready(function () {
            $('#tbBanco').DataTable({
                "searching": true,
                "aaSorting": [[2, "asc"]],
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
                },
                "columnDefs": [{
                    "targets": [-1],
                    "orderable": false
                },
                {
                "targets": [0],
                "visible": false
                }],

            });
        });
    </script>
@endsection
