@extends('layouts.pagina')
@section('content')
    <div class="col-12">
        <h1>
            Editar Horário
        </h1>
        @include('messages.alert')
    </div>
    <div class="col-12">
        <form action="{{route('horariotrabalho.update', $horariotrabalho)}}" method="post" class="form-row">
            @csrf
            @method('put')
            <div class="form-group col-12">
                <label class="font-weight-bold">Descrição</label>
                <input type="text" name="descricao" value="{{$horariotrabalho->descricao}}" id="descricao" class="form-control" placeholder="">
            </div>
            <div class="col-12 text-center">
                <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
        </form>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#descricao').mask('99:99 99:99 999:99');
        });
   </script>
@endsection
