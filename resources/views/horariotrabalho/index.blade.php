@extends('layouts.pagina')

@section('content')

<div class="col-12 text-center">
    @include('messages.alert')
    <a class="btn btn-primary my-3" href="{{route('horariotrabalho.create')}}" role="button">
        Novo Horário
    </a>
    <table class="table table-bordered">
        <thead>
            <tr class="bg-dark text-white text-center">

                <th>Horários de trabalho</th>
                <th width="10%">Ações</th>
            </tr>
        </thead>
        <tbody>
            @foreach($horariotrabalho as $ht)
            <tr>
                <td>{{$ht->descricao}}</td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-dark dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown">
                            Selecionar
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{route('horariotrabalho.edit',$ht)}}">Editar</a>
                            <!-- <a class="dropdown-item" href="#">Excluir</a> -->
                        </div>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
