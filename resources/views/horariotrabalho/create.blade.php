@extends('layouts.pagina')
@section('content')
    <div class="col-12">
        @include('messages.alert')

        <h1>
            Novo Horário de Trabalho
        </h1>
    </div>
    <div class="col-12">
        <form action="{{route('horariotrabalho.store')}}" method="post" class="form-row">
            @csrf
            <div class="form-group col-12">
                <label class="font-weight-bold">Horário de Trabalho</label>
                <input type="text" name="descricao" id="descricao" class="form-control" placeholder="">
            </div>
            <div class="col-12 text-center">
                <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
        </form>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#descricao').mask('99:99 99:99 999:99');
        });
   </script>

@endsection
