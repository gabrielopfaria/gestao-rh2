<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <title>
    @hasSection('title')
         @yield('title')
    @else
        Solicitação
    @endif
    </title>

    <style>
        @media print {
            .container {
                display: inline;
            }
        }
    </style>
</head>
<body>

<div class="container">

            <div class="row">
                <div class="col-12 text-center">
                    <img class="col-12 img-fluid" src="{{asset('img/banner-admin.png')}}">
                    <h1 class="text-center my-2">
                        @yield('titulo')
                    </h1>
                    <button class="btn btn-primary d-print-none" onClick="window.print();">Imprimir relatório</button> <br class="mb-2">
                </div>
            </div>


            @yield('content')

</div>


<div class="footer">
    <div style="margin-left: 5px;text-align: right;">
        <p style="font-size: 9px;">{{date("d/m/Y H:i:s")}}</p>
    </div>
</div>
