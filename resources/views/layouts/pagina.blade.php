<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{--CSRF Token--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Gestão RH</title>


    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/rh.js')}}"></script>
    <script src="{{asset('js/vue.js')}}" defer></script>


    <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
    @stack('js-import')
</head>

<body>
<div id="app" class="container">
    <div class="row">
        @include('layouts.cabecalho')
    </div>
    <div class="row">
        @include('layouts.menu')
    </div>

    <div class="row">
        @yield('content')
    </div>
    <div class="row">
        @include('layouts.rodape')
    </div>

    {{--        @include('partials.breadcrumbs',['breadcrumbs' => Breadcrumbs::generate()])--}}
</div>
</body>
