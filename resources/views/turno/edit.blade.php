@extends('layouts.pagina')

@section('content')

<div class="col-12">

@include('messages.alert')

    <div class="col-12 text-center">
        <h3>EDITAR TURNO</h3>
    </div>

    <form action="{{route('turno.update', $turno)}}" method="post">
        <input type="hidden" name="_method" value="put">
        {{ csrf_field() }}
            <div class="form-group col-12">
                <label>Turno *</label>
                <input name="descricao" id="descricao" value="{{$turno->descricao}}" type="text" class="form-control" placeholder="Digite o turno, apenas número" required />
            </div>


        <div class="row">
            <div class="form-group form-footer col-12 text-center" style="margin-bottom: 50px">

                <button class="btn btn-primary" type="submit">Salvar</button>

                <a class="btn btn-default" href="{{route('turno.index')}}">
                    Cancelar
                </a>
            </div>
        </div>

        </form>

</div>

<script>
    $(document).ready(function () {
        $('#descricao').mask('99:99 99:99 99:99 99:99');
    });
</script>
@endsection
