@extends('layouts.pagina')

@section('content')

<div class="col-12">

@include('messages.alert')

        <div class="text-center col-12">
                <a href="{{route('turno.create')}}" class="btn btn-large btn-primary m-3">
                    <b>Novo Turno</b>
                </a>
            </div>

            <table class="table table-striped table-bordered" id="tbTurno">
                <thead>
                <tr class="bg-dark text-white text-center">
                    <th>Id</th>
                    <th>Turno</th>
                    <th>Status</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($turno as $t)
                <tr>
                    <td>{{$t->id}}</td>
                    <td>{{$t->descricao}}</td>
                    <td>{{$t->deleted_at == NULL ? 'Ativo':'Inativo' }}</td>
                    <td>

                        <div class="dropdown open">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown">
                                        Selecione
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="triggerId">

                                        @if(!$t->deleted_at)
                                        <a class="dropdown-item" href="{{action('TurnoController@edit', $t)}}">Editar</a>

                                        <a class="dropdown-item"
                                        onclick="return confirm('Confirmar inativar')"
                                        href="{{url('turno/remove/'.$t->id)}}">Inativar</a>

                                        @else

                                        <a class="dropdown-item" href="{{action('TurnoController@ativar', $t->id)}}">Ativar</a>

                                        @endif

                                    </div>
                                </div>
                    </td>
                </tr>
                @endforeach

                </tbody>
            </table>

</div>

    <script>
        $(document).ready(function () {
            $('#tbTurno').DataTable({
                "searching": true,
                "aaSorting": [[2, "asc"]],
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
                },
                "columnDefs": [{
                    "targets": [-1],
                    "orderable": false
                },
                {
                "targets": [0],
                "visible": false
                }],

            });
        });
    </script>
@endsection
