<table class='table table-striped' style='font-size: 12px;'>
            <tr><th colspan="9" class="text-center"><h6 style="font-weight: bold">HISTÓRICO</h6></th></tr>
            <tr style='font-weight: bold;'>
            <th style='width:10px;'>Ano</th>
            <th style='width:50px;'>Data Expedição</th>
            <th style='width:10px;'>Ano Aquisitivo Inicial</th>
            <th style='width:10px;'>Ano Aquisitivo Final</th>
            <th style='width:50px;'>Data Inicial</th>
            <th style='width:50px;'>Data Final</th>
            <th style='width:50px;'>Data Retorno</th>
            <th style='width:10px;'>Qtd. Dias</th>
            <th style='width:100px;'>Ação</th>
            </tr>

    @foreach ($listagemFerias as $a)
            <tr>
                <td>{{$a->ano}}</td>
                <td>{{date('d/m/Y', strtotime( $a->data_expedicao))}}</td>
                <td>{{$a->ano_aquisitivo_inicial}}</td>
                <td>{{$a->ano_aquisitivo_final}}</td>
                <td>{{date('d/m/Y', strtotime( $a->data_inicial))}}</td>
                <td>{{date('d/m/Y', strtotime( $a->data_final))}}</td>
                <td>{{date('d/m/Y', strtotime( $a->data_retorno))}}</td>
                <td>{{$a->qtd_dias}}</td>
                <td>
                    <a href="{{action('FeriasController@edit',$a->id)}}" id="atualizar"
                        class="btn btn-warning btn-xs glyphicon glyphicon-edit"
                        title="Alterar Cadastro de Férias">Editar</a>
                    <a href="{{action('FeriasController@destroy',array($a->id,$funcionario->id))}}" id="excluir"
                        class="btn btn-danger btn-xs glyphicon glyphicon-trash"
                        title="Excluir Cadastro de Férias">Excluir</a>
                </td>
            </tr>
    @endforeach

</table>
