@extends('layouts.pagina')

@section('js-import')
<script src="{{asset('js/ferias.js')}}"></script>
@endsection

@section('content')

    <div class=col-12 style="margin-top:10px">

         @include('messages.alert')

        <div class="form-group col-12" style="margin-left: 0px;margin-bottom: 30px;background-color: #F0FFF0">
            <div class="form-group col-12 mt-3"><b>Funcionário: </b>{{$funcionario->matricula}} - {{$funcionario->nome}}</div>
            <div class="form-group col-12"><b>Lotação: </b>{{$funcionario->lotacao? $funcionario->lotacao->codigo. '-' . $funcionario->lotacao->descricao : ''}}</div>
        </div>

        <form action="{{--route('ferias.store')--}}" method="post">

            {{ csrf_field() }}
           <input type="hidden" name="func_matricula" value="{{$funcionario->matricula}}">
           <input type="hidden" name="funcionario_id" value="{{$funcionario->id}}">


            <div class="row">
                <div class="col-12 col-md-6">
                    <label>Ano<span class="obrigatorio">*</span></label>
                    <input name="ano" id="ano" class="form-control mb-2 ano" value="{{old('ano',$ferias->ano)}}"
                        minlength="4" required/>
                </div>
                <div class="col-12 col-md-6">
                    <label>Data de Expedição<span class="obrigatorio">*</span></label>
                    <input name="data_expedicao" id="data_expedicao" class="form-control mb-2 data"
                        value="{{old('data_expedicao',$ferias->data_expedicao)}}" maxlength="10" required/>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-6">
                    <label>Ano Aquisitivo Inicial<span class="obrigatorio">*</span></label>
                    <input name="ano_aquisitivo_inicial" id="ano_aquisitivo_inicial" class="form-control mb-2 ano"
                        value="{{old('ano_aquisitivo_inicial',$ferias->ano_aquisitivo_inicial)}}" onblur="SomarAno()"
                        minlength="4" required/>
                </div>
                <div class="col-12 col-md-6">
                    <label>Ano Aquisitivo Final<span class="obrigatorio">*</span></label>
                    <input name="ano_aquisitivo_final" id="ano_aquisitivo_final" class="form-control mb-2 ano"
                        value="{{old('ano_aquisitivo_final',$ferias->ano_aquisitivo_final)}}" minlength="4"
                        readonly="true" required/>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-6">
                    <label>Data Inicial<span class="obrigatorio">*</span></label>
                    <input name="data_inicial" id="data_inicial" class="form-control mb-2 data"
                        value="{{old('data_inicial',$ferias->data_inicial)}}" onblur="CalcularData()" maxlength="10"
                        required/>
                </div>
                <div class="col-12 col-md-6">
                    <label>Qtd. dias<span class="obrigatorio">*</span></label>
                    <input name="qtd_dias" id="qtd_dias" class="form-control mb-2 qtd_dias"
                        value="{{old('qtd_dias',$ferias->qtd_dias)}}" onblur="CalcularData()" maxlength="4"
                        required/>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-6">
                    <label>Data Final<span class="obrigatorio">*</span></label>
                    <input name="data_final" id="data_final" class="form-control mb-2"
                        value="{{old('data_final',$ferias->data_final)}}" maxlength="10" readonly="true"
                        required/>
                </div>

                <div class="col-12 col-md-6">
                    <label>Data Retorno<span class="obrigatorio">*</span></label>
                    <input name="data_retorno" id="data_retorno" class="form-control mb-2"
                        value="{{old('data_retorno',$ferias->data_retorno)}}" maxlength="10"
                        readonly="true" required/>
                </div>
            </div>


            <div class="form-group form-footer col-12 text-center mt-4 mb-5">

                <button class="btn btn-primary" type="submit">Salvar</button>

                <a class="btn btn-default" href="{{action('FeriasController@index')}}">
                    Cancelar
                </a>
            </div>

        </form>


        {{--@include('ferias.cadastro_table_historico')--}}


    </div>

@endsection

