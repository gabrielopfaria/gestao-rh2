@extends('layouts.pagina')

@section('content')

    <div class=col-12 style="margin-top:10px">

    @include('messages.alert')

        <h3>Listagem - Cadastro de Férias</h3>
        <br>

        <a class="btn btn-alert" href="ferias/1">Cadastrar Férias</a>

        <table class="table table-striped table-bordered" id="tbFerias">
            <thead>
                    <tr>
                        <th>Id</th>

                        <th>Matrícula</th>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Data de Admissão</th>
                        <th>CPF</th> {{--cpf para consulta--}}
                        <th>Ações</th>
                    </tr>
            </thead>
        </table>

    </div>


    <script>
      $(document).ready(function() {

        var table = $('#tbFerias').DataTable( {
          "processing": true,
          "serverSide": true,
          "searching":true,
          "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
          },
          "ajax": '{{ route ("resultFuncionario")}}',

          columns: [
            { data: 'id', name: 'id',"bVisible": false},
            { data: 'matricula', name: 'matricula'},
            { data: 'nome', name: 'nome'},
            { data: 'cpf_format', name: 'cpf_format'},
            { data: 'dt_admissao', name: 'dt_admissao'},
            { data: 'cpf', name: 'cpf',"bVisible": false},

            {
              data: function (data, type, row, meta) {

              $html = "";


                /*$html = '&nbsp;<a class="btn btn-warning btn-xs glyphicon glyphicon-pencil"' +
                  'href="ferias/edit/' + data.id + '" title="Cadastrar Férias" >Férias</a>';*/

                  $html = '&nbsp;<a class="btn btn-warning btn-xs glyphicon glyphicon-pencil"' +
                  'href="ferias/' + data.id + '" title="Cadastrar Férias" >Férias</a>';


                  /*

              $html +=  "<div class='dropdown open'>"+
                             "<button class='btn btn-secondary dropdown-toggle' type='button' id='triggerId' data-toggle='dropdown'>"+
                                    "Selecione"+
                                    "<div class='dropdown-menu' aria-labelledby='triggerId'>";

              $html +=    '<a class="btn" href="/' + data.id + '">Cadastrar Férias</a>';



              $html +=              "</div>"+
                              "</button>"+
                        "</div>"; */

              return $html;

              }

            }
          ],
          "columnDefs": [{
            "targets": [-1],
            "orderable": false
          },
          ],
          "aaSorting": [[3, "asc" ]],//ordenar indice
        } );
      } );


    </script>

@endsection
