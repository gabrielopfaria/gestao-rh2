@extends('layouts.pagina')

@section('content')

<div class="col-12">

@include('messages.alert')

        <div class="text-center col-12">
                <a href="{{route('escolaridade.create')}}" class="btn btn-large btn-primary m-3">
                    <b>Nova Escolaridade</b>
                </a>
            </div>

            <table class="table table-striped table-bordered" id="tbEscolaridade">
                <thead>
                <tr class="bg-dark text-white text-center">
                    <th>Id</th>
                    <th>Descricao</th>
                    <th>Status</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($escolaridades as $e)
                <tr>
                    <td>{{$e->id}}</td>
                    <td>{{$e->descricao}}</td>
                    <td>{{$e->deleted_at == NULL ? 'Ativo':'Inativo' }}</td>
                    <td>

                        <div class="dropdown open">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown">
                                        Selecione
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="triggerId">

                                    @if(!$e->deleted_at)
                                        <a class="dropdown-item" href="{{action('EscolaridadeController@edit', $e)}}">Editar</a>
                                        <a class="dropdown-item"
                                        onclick="return confirm('Confirmar inativar')"
                                        href="{{url('escolaridade/remove/'.$e->id)}}">Inativar</a>

                                        @else

                                            <a class="dropdown-item" href="{{action('EscolaridadeController@ativar', $e->id)}}">Ativar</a>

                                        @endif

                                    </div>
                                </div>
                    </td>
                </tr>
                @endforeach

                </tbody>
            </table>

</div>

    <script>
        $(document).ready(function () {
            $('#tbEscolaridade').DataTable({
                "searching": true,
                "aaSorting": [[1, "asc"]],
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
                },
                "columnDefs": [{
                    "targets": [-1],
                    "orderable": false
                },
                {
                "targets": [0],
                "visible": false
                }
            ],
            });
        });
    </script>
@endsection
