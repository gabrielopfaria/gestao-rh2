@extends('layouts.pagina')

@section('content')

<div class="col-12">

@include('messages.alert')

@if($escolaridade->exists)
    <div class="col-12 text-center">
        <h3>EDITAR ESCOLARIDADE</h3>
    </div>

    <form action="{{route('escolaridade.update', $escolaridade)}}" method="post">
        <input type="hidden" name="_method" value="put">

@else

    <div class="col-12 text-center">
        <h3>CADASTRO DE ESCOLARIDADE</h3>
    </div>

    <form action="{{route('escolaridade.store')}}" method="post">
@endif
        {{ csrf_field() }}

            <div class="form-group col-12">
                <label>Descrição *</label>
                <input name="descricao" id="descricao" value="{{old('descricao',$escolaridade->descricao)}}" type="text" class="form-control" placeholder="Digite o nome da escolaridade" required />
            </div>


        <div class="row">
            <div class="form-group form-footer col-12 text-center" style="margin-bottom: 50px">

                <button class="btn btn-primary" type="submit">Salvar</button>

                <a class="btn btn-default" href="{{route('escolaridade.index')}}">
                    Cancelar
                </a>
            </div>
        </div>

        </form>

</div>
@endsection



