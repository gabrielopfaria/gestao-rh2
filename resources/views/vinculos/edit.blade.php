@extends('layouts.pagina')
@section('content')
    <div class="col-12">
        <h1>
            Editar vinculo
        </h1>
        @include('messages.alert')
    </div>
    <div class="col-12">
        <form action="{{route('vinculo.update',$vinculo)}}" method="post" class="form-row">
            @csrf
            @method('put')
            <div class="form-group col-4">
                <label class="font-weight-bold">Sigla</label>
                <input type="text" name="sigla" value="{{$vinculo->sigla}}" id="sigla" class="form-control" placeholder="">
            </div>
            <div class="form-group col-8">
                <label class="font-weight-bold">Descrição</label>
                <input type="text" name="descricao" value="{{$vinculo->descricao}}" id="descricao" class="form-control" placeholder="">
            </div>
            <div class="col-12 text-center">
                <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
        </form>
    </div>

@endsection
