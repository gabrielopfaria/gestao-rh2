@extends('layouts.pagina')
@section('content')
<div class="col-12">
    <h1>Vínculos</h1>
</div>
<div class="col-12 text-center">
    <a class="btn btn-primary my-3" href="{{route('vinculo.create')}}" role="button">
        Novo vínculo
    </a>

    @include('messages.alert')
    <table class="table table-bordered">
        <thead>
            <tr class="bg-dark text-white text-center">
                <th>Sigla</th>
                <th>Descrição</th>
                <th width="10%">Ações</th>
            </tr>
        </thead>
        <tbody>
            @foreach($vinculos as $v)
            <tr>
                <td>{{$v->sigla}}</td>
                <td>{{$v->descricao}}</td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-dark dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown">
                            Selecionar
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{route('vinculo.edit',$v)}}">Editar</a>
                            <!-- <a class="dropdown-item" href="#">Excluir</a> -->
                        </div>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
