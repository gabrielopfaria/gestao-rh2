@extends('layouts.pagina')

@section('content')

<div class="col-12">

@include('messages.alert')


        <div class="text-center col-12">
                <a href="{{route('grauparentesco.create')}}" class="btn btn-large btn-primary m-3">
                    <b>Novo Grau de Parentesco</b>
                </a>
            </div>

            <table class="table table-striped table-bordered" id="tbGrauParentesco">
                <thead>
                <tr class="bg-dark text-white text-center">
                    <th>Id</th>
                    <th>Descrição</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($grauparentesco as $g)
                <tr>
                    <td>{{$g->id}}</td>
                    <td>{{$g->descricao}}</td>
                    <td>

                        <div class="dropdown open">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown">
                                        Selecione
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="triggerId">
                                        <a class="dropdown-item" href="{{action('GrauParentescoController@edit', $g)}}">Editar</a>
                                        <a class="dropdown-item"
                                        onclick="return confirm('Confirmar exclusão')"
                                        href="{{url('grauparentesco/remove/'.$g->id)}}">Excluir</a>
                                    </div>
                                </div>
                    </td>
                </tr>
                @endforeach

                </tbody>
            </table>

</div>

    <script>
        $(document).ready(function () {
            $('#tbGrauParentesco').DataTable({
                "searching": true,
                "aaSorting": [[1, "asc"]],
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
                },
                "columnDefs": [{
                    "targets": [-1],
                    "orderable": false
                },
                {
                "targets": [0],
                "visible": false
                }],

            });
        });
    </script>
@endsection
