{{ csrf_field() }}

<input type="hidden" value="{{$solicitacao->id}}" name="solicitacao_id" id="solicitacao_id"/>

<div class="row">
    <div class="col-12 col-md-4">
                <label>Avaliação<span class="obrigatorio">*</span> </label>
                <select class="form-control mb-3" name="status" id="status" required>
                    <option value="" selected>-- Selecione --</option>
                    <option value="1" {{(old('status') == '1'?'selected':'')}}>Deferido</option>
                    <option value="0" {{(old('status') == '0'?'selected':'')}}>Indeferido</option>
                </select>
    </div>
</div>


<div class="row" style="display:none" id="divIndeferido">

        <div class="col-12">
            <label for="motivo_indeferido">Motivo Indeferimento <span class="obrigatorio">*</span> </label>
            <textarea class="form-control mb-2" name="motivo_indeferido" id="motivo_indeferido" rows="3" >{{old('motivo_indeferido')}}</textarea>
        </div>

</div>
