<div class="form-group col-12" style="margin-left: 0px;margin-bottom: 20px;background-color: #F0FFF0; padding:6px">
            <div class="form-group col-12 mt-3"><b>Funcionário: </b>{{$solicitacao->funcionario->matricula}} - {{$solicitacao->funcionario->nome}}</div>
            <div class="form-group col-12"><b>Lotação: </b>{{$solicitacao->funcionario->lotacao? $solicitacao->funcionario->lotacao->codigo. ' - ' . $solicitacao->funcionario->lotacao->descricao : ''}}</div>
    </div>

    <div class="row">
                <div class="col-12 col-md-4">
                    <label>Data Inicial</label>
                    <input name="data_inicial" id="data_inicial" class="form-control mb-2"
                        value="{{date('d/m/Y', strtotime($solicitacao->data_inicial)) }}" readonly/>
                </div>
                <div class="col-12 col-md-4">
                    <label>Data Final</label>
                    <input name="data_final" id="data_final" class="form-control mb-2"
                        value="{{date('d/m/Y', strtotime($solicitacao->data_final))}}" readonly/>
                </div>
                <div class="col-12 col-md-4">
                    <label>Qtde Dias</label>
                    <input name="qtd_dias" id="qtd_dias" class="form-control mb-2"
                        value="{{App\Utils\DateUtil::qtdDias($solicitacao->data_inicial,  $solicitacao->data_final)}}" readonly/>
                </div>
    </div>

    <div class="row">
        <div class="col-12">
            <label for="observacao">Observações </label>
            <textarea class="form-control mb-2" name="observacao" id="observacao" rows="3" readonly>{{$solicitacao->observacao}}</textarea>
        </div>
    </div>
