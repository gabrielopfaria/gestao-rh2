<div class="row">
                <div class="col-12 col-md-6">
                    <label>Ano<span class="obrigatorio">*</span></label>
                    <input name="ano" id="ano" class="form-control mb-2 ano"  value="{{old('ano',$ferias->ano)}}"
                        minlength="4" />
                </div>
                <div class="col-12 col-md-6">
                    <label>Data de Expedição<span class="obrigatorio">*</span></label>
                    <input name="data_expedicao" id="data_expedicao" class="form-control mb-2 data"
                    value="{{old('data_expedicao',$ferias->data_expedicao)}}" maxlength="10" />
                </div>
 </div>

 <div class="row">
                <div class="col-12 col-md-6">
                    <label>Ano Aquisitivo Inicial<span class="obrigatorio">*</span></label>
                    <input name="ano_aquisitivo_inicial" id="ano_aquisitivo_inicial" class="form-control mb-2 ano"
                    value="{{old('ano_aquisitivo_inicial',$ferias->ano_aquisitivo_inicial)}}"  onblur="SomarAno()"
                        minlength="4" />
                </div>
                <div class="col-12 col-md-6">
                    <label>Ano Aquisitivo Final<span class="obrigatorio">*</span></label>
                    <input name="ano_aquisitivo_final" id="ano_aquisitivo_final" class="form-control mb-2 ano"
                    value="{{old('ano_aquisitivo_final',$ferias->ano_aquisitivo_final)}}" minlength="4"
                        />
                </div>
</div>

<div class="row">
<div class="col-12 col-md-6">
                    <label>Data Retorno<span class="obrigatorio">*</span></label>
                    <input name="data_retorno" id="data_retorno" class="form-control mb-2 data"
                        value="{{old('data_retorno',$ferias->data_retorno)}}" maxlength="10"
                        />
                </div>
</div>
