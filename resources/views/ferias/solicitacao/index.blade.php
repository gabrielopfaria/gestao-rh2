@extends('layouts.pagina')

@section('content')

    <div class=col-12 style="margin-top:10px">

        @include('messages.alert')

        <h3>Listagem - Solicitação de Férias</h3>
        <br>

        <div class="form-group col-12" style="margin-left: 0px;margin-bottom: 20px;background-color: #F0FFF0; ">
            <div><b>Solicitante: </b>{{ $funcionario->matricula }} - {{ $funcionario->nome }}</div>
            <div><b>Lotação:
                </b>{{ $funcionario->lotacao ? $funcionario->lotacao->codigo . ' - ' . $funcionario->lotacao->descricao : '' }}
            </div>
            {{-- <div><b>Chefe imediato atual: </b>{{$funcionario->superior?$funcionario->superior->matricula . ' - ' . $funcionario->superior->nome : '-'}}</div> --}}
        </div>


        {{-- @if (!$user_cpf || !$funcionario || !$funcionario->superior_id)
            <div class="col-12" style="margin-left: 0px;margin-bottom: 10px;background-color: #fcefa1; padding:3px">
                <span> Para realizar solicitações:</span>

                <ul>
                    @if (!$user_cpf)
                        <li>O usuário logado deve estar vinculado a um CPF.</li>
                    @endif
                    @if (!$funcionario)
                        <li>O CPF vinculado ao usuário logado deve estar cadastrado em 'Funcionários'.</li>
                    @endif
                    @if (!$funcionario->superior_id)
                        <li>O Funcionário deve estar vinculado a um chefe imediato, para realização de aprovação das férias.
                        </li>
                    @endif
                </ul>
            </div>
        @endif --}}
        <div class="row mb-5">
            <div class="col-10">
                <form action="{{ action('FeriasSolicitacaoController@create') }}" method="get">
                    {{ csrf_field() }}
                    <label>Selecione um funcionário para solicitar férias:</label>
                    <select name="equipe_id" id="equipe_id" class="form-control" required>
                        <option value="" selected>-- Selecione --</option>
                        @foreach ($equipe as $e)
                            <option value="{{ $e->id }}">{{$e->matricula}} - {{ $e->nome }}</option>
                        @endforeach

                    </select>
                    <button class="btn btn-primary" type="submit">Solicitar Férias</button>
                </form>
            </div>
        </div>

        <table class="table table-striped table-bordered" id="tbSolFerias">
            <thead>
                <tr class="bg-dark text-white text-center">
                    <th>Id</th>
                    <th>REF</th>
                    <th>Data da Solicitação</th>
                    <th>Status</th>
                    <th>Data Início</th>
                    <th>Data Fim</th>
                    <th>Qtde Dias</th>
                    <th>Funcionáro</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($solicitacoes as $s)
                    <tr>
                        <td>{{ $s->id }}</td>
                        <td>{{ $s->getreferencia() }}</td>
                        <td>{{ date('d/m/Y H:i', strtotime($s->created_at)) }}</td>
                        <td {{ $s->getColor($s->id) }}>{{ $s->status->status }}</td>
                        <td>{{ date('d/m/Y', strtotime($s->data_inicial)) }}</td>
                        <td>{{ date('d/m/Y', strtotime($s->data_final)) }}</td>
                        <td>{{ App\Utils\DateUtil::qtdDias($s->data_inicial, $s->data_final) }}</td>

                        <td>{{ $s->funcionario ? $s->funcionario->matricula . ' - ' . $s->funcionario->nome : '' }}</td>
                        <td>

                            <div class="dropdown open">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="triggerId"
                                    data-toggle="dropdown">
                                    Selecione
                                </button>
                                <div class="dropdown-menu" aria-labelledby="triggerId">

                                    @if ($s->status_id == 1)
                                        <form class="dropdown-item"
                                            action="{{ action('FeriasSolicitacaoController@enviarAprovacaoSuperior') }}"
                                            method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" value="{{ $s->id }}" name="solicitacao_id">
                                            <button class="btn btn-link">Enviar para RH</button>
                                        </form>



                                        {{-- <a class="dropdown-item" target="new" href="{{ route('edit', $s->id) }}">Alterar
                                            Data</a> --}}
                                        <input type="hidden" value="{{ $s->id }}" name="solicitacao_id">


                                        {{-- <form class="dropdown-item" target="new" action="{{route('FeriasSolicitacaoController@edit',["solicitacao_id"=>$s->id])}}" method="get">
                                        <button class="btn btn-link">Alterar Data</button>
                                        {{ csrf_field() }}
                                    <input type="hidden" value="{{$s->id}}" name="solicitacao_id">
                                    <button class="btn btn-link">Alterar Data</button>
                                </form> --}}
                                    @endif


                                    <a class="dropdown-item" target="new"
                                        href="{{ route('historico', $s->id) }}">Visualizar
                                        Pedido</a>


                                </div>
                            </div>

                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>

    <script>
        $(document).ready(function() {
            $('#tbSolFerias').DataTable({
                "searching": true,
                "aaSorting": [
                    [1, "asc"]
                ],
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
                },
                "columnDefs": [{
                        "targets": [-1],
                        "orderable": false
                    },
                    {
                        "targets": [0],
                        "visible": false
                    },
                    {
                        width: 70,
                        targets: 4
                    },
                    {
                        width: 70,
                        targets: 5
                    },

                ],

            });
        });

    </script>

@endsection
