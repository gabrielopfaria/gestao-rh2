@extends('layouts.pagina')

@push('js-import')
<script src="{{asset('js/ferias.js')}}" defer></script>
@endpush

@section('content')

<div class=col-12 style="margin-top:10px">

    @include('messages.alert')

    <h4>Solicitar Férias</h4>

    <div class="form-group col-12" style="margin-left: 0px;margin-bottom: 30px;background-color: #F0FFF0; padding:6px">
        <div class="form-group col-12 mt-3"><b>Solicitante: </b>{{$funcionario->matricula}} - {{$funcionario->nome}}</div>
        <div class="form-group col-12"><b>Lotação: </b>{{$funcionario->lotacao? $funcionario->lotacao->codigo. ' - ' . $funcionario->lotacao->descricao : ''}}</div>
        <div class="form-group col-12"><b>Funcionário: </b>{{$solicitado->nome}}</div>
    </div>


    <form action="{{action('FeriasSolicitacaoController@store')}}" method="post">

        {{ csrf_field() }}

        <input type="hidden" value="{{$solicitado->id}}" name="solicitado_id" id="solicitado_id"/>
        <input type="hidden" value="{{$funcionario->id}}" name="superior_id" id="superior_id">

        <div class="row">
                <div class="col-12 col-md-6">
                    <label>Data Inicial<span class="obrigatorio">*</span></label>
                    <input name="data_inicial" id="data_inicial" class="form-control mb-2 data"
                        value="{{old('data_inicial',$solicitacao->data_inicial)}}" onblur="CalcularDataSol()" maxlength="10"
                        required autofocus/>
                </div>
                <div class="col-12 col-md-6">
                    <label>Qtd. dias<span class="obrigatorio">*</span></label>
                    <select class="form-control mb-3" name="qtd_dias" id="qtd_dias" onchange="CalcularDataSol()"  required>
                        <option value="" selected>-- Selecione --</option>
                        <option value="10" {{(old('qtd_dias') == '1'?'selected':'')}}>10</option>
                        <option value="15" {{(old('qtd_dias') == '0'?'selected':'')}}>15</option>
                        <option value="20" {{(old('qtd_dias') == '2'?'selected':'')}}>20</option>
                        <option value="30" {{(old('qtd_dias') == '2'?'selected':'')}}>30</option>
                    </select>
                </div>

                <div class="col-12 col-md-6">
                    <label>Data Final<span class="obrigatorio">*</span></label>
                    <input name="data_final" id="data_final" class="form-control mb-2 data"
                        value="{{old('data_final',$solicitacao->data_final)}}" maxlength="10" readonly="true"
                        required/>
                </div>
            </div>


        <div class="row">
            <div class="col-12">
                    <label for="observacao">Observações </label>
                    <textarea class="form-control" name="observacao" id="observacao" rows="3">{{$solicitacao->observacao}}</textarea>
            </div>
        </div>

        <div class="form-group form-footer col-12 text-center mt-4 mb-5">

                    <button class="btn btn-primary" type="submit">Salvar</button>

                    <a class="btn btn-default" href="{{action('FeriasSolicitacaoController@index')}}">
                        Cancelar
                    </a>
        </div>

</form>
</div>

@endsection
