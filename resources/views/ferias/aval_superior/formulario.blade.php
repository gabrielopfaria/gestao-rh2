@extends('layouts.pagina')

@section('content')

<div class=col-12 style="margin-top:10px">

    @include('messages.alert')

    <h4>Avaliar Férias</h4>


    @include('ferias.aprovacao.dados_solicitacao')


    <form action="{{action('FeriasAvaliacaoSuperiorController@store')}}" method="post">


         @include('ferias.aprovacao.dados_aprovacao')

        <div class="form-group form-footer col-12 text-center mt-4 mb-5">

                    <button class="btn btn-primary" type="submit">Salvar</button>

                    <a class="btn btn-default" href="{{action('FeriasAvaliacaoSuperiorController@index')}}">
                        Cancelar
                    </a>
        </div>

    </form>



</div>


@include('ferias.aprovacao.script')


@endsection
