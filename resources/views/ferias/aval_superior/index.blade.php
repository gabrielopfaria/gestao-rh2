@extends('layouts.pagina')

@section('content')

<div class=col-12 style="margin-top:10px">

    @include('messages.alert')

    <h3>Listagem - Avaliação de Férias</h3>
    <br>

    <div class="form-group col-12 p-2" style="margin-left: 0px;margin-bottom: 20px;background-color: #F0FFF0; ">
        <div><b>Avaliador: </b>{{$chefeimediato->matricula}} - {{$chefeimediato->nome}}</div>
    </div>

    <table class="table table-striped table-bordered" id="tbAvalSupFerias">
                <thead>
                <tr class="bg-dark text-white text-center">
                    <th>Id</th>
                    <th>REF</th>
                    <th>Data da Solicitação</th>
                    <th>Funcionário</th>
                    <th>Status</th>

                    <th>Data Início</th>
                    <th>Data Fim</th>
                    <th>Qtde Dias</th>

                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($solicitacoes as $s)
                <tr>
                    <td>{{$s->id}}</td>
                    <td>{{$s->getreferencia()}}</td>
                    <td>{{date('d/m/Y H:i', strtotime( $s->created_at))}}</td>
                    <td>{{$s->funcionario->matricula . ' - ' . $s->funcionario->nome}}</td>
                    <td>-- Aguardando Análise --</td>

                    <td>{{date('d/m/Y', strtotime( $s->data_inicial))}}</td>
                    <td>{{date('d/m/Y', strtotime( $s->data_final))}}</td>
                    <td>{{App\Utils\DateUtil::qtdDias($s->data_inicial,  $s->data_final)}}</td>

                    <td>

                        <div class="dropdown open">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown">
                                Selecione
                            </button>
                            <div class="dropdown-menu" aria-labelledby="triggerId">
                                @if($s->status_id == 2)
                                    <a class="dropdown-item" href="{{action('FeriasAvaliacaoSuperiorController@edit', $s->id)}}">Avaliar</a>
                                @endif
                            </div>
                        </div>

                    </td>
                </tr>
                @endforeach

                </tbody>
            </table>
</div>

<script>
        $(document).ready(function () {
            $('#tbAvalSupFerias').DataTable({
                "searching": true,
                "aaSorting": [[1, "asc"]],
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
                },
                "columnDefs": [{
                    "targets": [-1],
                    "orderable": false
                },
                {
                "targets": [0],
                "visible": false
                }],

            });
        });
</script>

@endsection

