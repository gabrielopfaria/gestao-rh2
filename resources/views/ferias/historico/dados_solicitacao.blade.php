<div class="col-12 my-2 px-5">
                    <h3>Solicitação de Férias</h3>
                    <ul class="list-group">
                        <li class="list-group-item">

                        <div class="col-12"><b>Solicitação:</b> {{$solicitacao->getreferencia()}}</div>
                        <div class="col-12"><b>Status Atual:</b> {{$solicitacao->status->status}}</div>
                        <div class="col-12"><b>Data Solicitação:</b> {{date('d/m/Y', strtotime( $solicitacao->created_at))}}</div>

                        <div class="col-12"><b>Período de Férias:</b>
                           {{ date('d/m/Y', strtotime( $solicitacao->data_inicial)) . ' - '.
                              date('d/m/Y', strtotime( $solicitacao->data_final))
                           }}</div>

                        <div class="col-12"><b>Qtde Dias:</b> {{App\Utils\DateUtil::qtdDias($solicitacao->data_inicial,  $solicitacao->data_final)}}</div>

                        <div class="col-12"><b>Observação:</b> {{$solicitacao->observacao}}</div>

                        </li>
                    </ul>
</div>
