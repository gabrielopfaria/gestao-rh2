@extends('layouts.pagina-view')

@section('titulo') Histórico @endsection

@section('content')


            @include('ferias.historico.dados_funcionario')

            @include('ferias.historico.dados_solicitacao')


            @if($avaliacao->count()>0)

                <div class="col-12 my-2 px-5">
                    <h3>Avaliação</h3>
                    <ul class="list-group">
                        <li class="list-group-item">
                        @foreach($avaliacao as $b)

                        <div class="col-12">Avaliação do {{$b->gettipoAvaliador()}}</div>

                        <div class="col-12"><b>Avaliador:</b> {{$b->avaliador->matricula . ' - ' . $b->avaliador->nome}}</div>
                        <div class="col-12"><b>Data Avaliação:</b> {{date('d/m/Y', strtotime( $b->created_at))}}</div>
                        <div class="col-12"><b>Avaliação:</b> {{$b->getavaliacao()}}</div>

                        @if($b->motivo_indeferido)
                        <div class="col-12"><b>Motivo indeferimento:</b> {{$b->motivo_indeferido}}</div>
                        @endif

                        <br>

                        @endforeach
                        </li>
                    </ul>
                </div>

            @endif


@endsection
