@extends('layouts.pagina')

@section('content')

<div class=col-12 style="margin-top:10px">

    @include('messages.alert')

    <h3>Listagem - Avaliação de Férias RH</h3>
    <br>

    <small style="color: blue">* Listagem de Solicitações Em Aberto para Análise do RH
    ou analisadas até a data corrente.
    </small>

    <table class="table table-striped table-bordered" id="tbAvalRHFerias">
                <thead>
                <tr class="bg-dark text-white text-center">
                    <th>Id</th>
                    <th>Matrícula</th>
                    <th>Funcionário</th>
                    <th>Lotação</th>
                    <th>Qtd Solicitações Para análise</th>
                    <th>Data início mais próxima</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tbody>
            </table>
</div>

<script>
      $(document).ready(function() {

        var table = $('#tbAvalRHFerias').DataTable( {
          "processing": true,
          "serverSide": true,
          "searching":true,
          "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
          },
          "ajax": '{{ route ("resultAvalRHFerias")}}',

          columns: [
            { data: 'id', name: 'id', "bVisible": false},
            { data: 'matricula', name: 'matricula'},
            { data: 'nome', name: 'nome'},
            { data: 'lotacao', name: 'lotacao'},
            { data: 'qtde_sol', name: 'qtde_sol'},
            { data: 'dtinicio_proxima', name: 'dtinicio_proxima'},
            {
              data: function (data, type, row, meta) {

              $html = "";



              $html += '<div class="dropdown open">';
              $html +=    '<button class="btn btn-secondary dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown">';
              $html +=                   'Selecione';
              $html +=    '</button>';
              $html +=     '<div class="dropdown-menu" aria-labelledby="triggerId">';

              $html +=        '<a class="dropdown-item" href="/ferias/avaliacao/rh/'+data.id+'">Avaliar Férias</a>';
              $html +=      '</div>';
              $html +=  '</div>';

              return $html;


              }

            }

          ],
          "columnDefs": [{
            "targets": [-1],
            "orderable": false
          },
         /* {
            "targets": [0],
            "visible": false
          },*/
          ],
          "aaSorting": [[3, "asc" ]],//ordenar indice
        } );
      } );


    </script>


@endsection
