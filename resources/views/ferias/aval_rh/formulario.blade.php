@extends('layouts.pagina')

@section('js-import')
<script src="{{asset('js/ferias.js')}}"></script>
@endsection

@section('content')

<div class=col-12 style="margin-top:10px">

    @include('messages.alert')

    <h4>Avaliar Férias</h4>


    @include('ferias.aprovacao.dados_solicitacao')


    <form action="{{action('FeriasAvaliacaoRHController@store')}}" method="post">


         @include('ferias.aprovacao.dados_aprovacao')


         <div style="display:none" id="divDeferido">

                @include('ferias.aprovacao.dados_deferidorh')

          </div>


        <div class="form-group form-footer col-12 text-center mt-4 mb-5">

                    <button class="btn btn-primary" type="submit">Salvar</button>

                    <a class="btn btn-default" href="{{action('FeriasAvaliacaoRHController@index')}}">
                        Cancelar
                    </a>
        </div>

    </form>



</div>


<script>

$(document).ready(function() {

        $('#status').on('change', function () {

            now = new Date;

            diaAtual =  dataAtualFormatada();

            if(this.value == ""){
                $('#divIndeferido').hide();
                $('#motivo_indeferido').val("");

                $('#divDeferido').hide();
                $('#ano_aquisitivo_inicial').val("");
                $('#ano_aquisitivo_final').val("");
                $('#ano').val(now.getFullYear());
                $('#data_expedicao').val(diaAtual);
                $('#data_retorno').val("");
            }
            else if(this.value == 0){

                $('#divIndeferido').show();

                $('#divDeferido').hide();
                $('#ano_aquisitivo_inicial').val("");
                $('#ano_aquisitivo_final').val("");
                $('#ano').val(now.getFullYear());
                $('#data_expedicao').val(diaAtual);
                $('#data_retorno').val("");

            }else{
                $('#divIndeferido').hide();
                $('#motivo_indeferido').val("");

                $('#divDeferido').show();
            }

        });
});

</script>


@endsection
