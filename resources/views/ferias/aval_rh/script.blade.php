<script>
        $(document).ready(function () {
            $('#tbSolFerias').DataTable({
                "searching": true,
                "aaSorting": [[1, "asc"]],
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
                },
                "columnDefs": [{
                    "targets": [-1],
                    "orderable": false
                },
                {
                "targets": [0],
                "visible": false
                },
                { width: 70, targets: 4 },
                { width: 70, targets: 5 },

                ],

            });

            $('#tbAvalFerias').DataTable({
                "searching": true,
                "aaSorting": [[1, "asc"]],
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
                },
                "columnDefs": [{
                    "targets": [-1],
                    "orderable": false
                },
                {
                "targets": [0],
                "visible": false
                },
                { width: 70, targets: 4 },
                { width: 70, targets: 5 },

                ],

            });



        });
</script>
