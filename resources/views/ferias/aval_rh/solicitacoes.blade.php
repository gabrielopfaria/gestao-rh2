@extends('layouts.pagina')

@section('content')

    <div class=col-12 style="margin-top:10px">

    @include('messages.alert')

    <div class="form-group col-12" style="margin-left: 0px;margin-bottom: 30px;background-color: #F0FFF0">
    <div class="form-group col-12 mt-3"><b>Funcionário: </b>{{$funcionario->matricula}} - {{$funcionario->nome}}</div>
    <div class="form-group col-12"><b>Lotação: </b>{{$funcionario->lotacao? $funcionario->lotacao->codigo. '-' . $funcionario->lotacao->descricao : ''}}</div>
    </div>

    <a class="btn btn-primary" href="{{action('FeriasAvaliacaoRHController@index')}}">
                        Voltar
    </a>


    <div class="text-center" style="background-color:#8AA3B1; color:white">
    <h5>Solicitações de Férias para Aprovação:</h5>
    </div>

    @include('ferias.aval_rh.tables.table_avaliar')



    <div class="text-center" style="background-color:#8AA3B1; color:white; margin-top:100px;">
    <h5>Solicitações de Férias Avaliadas:</h5>
    </div>

    @include('ferias.aval_rh.tables.table_avaliada')


    @include('ferias.aval_rh.script')


@endsection
