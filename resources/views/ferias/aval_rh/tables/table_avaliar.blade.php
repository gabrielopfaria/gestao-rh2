<table class="table table-striped table-bordered" id="tbAvalFerias">
                <thead>
                <tr class="bg-dark text-white text-center">
                    <th>Id</th>
                    <th>REF</th>
                    <th>Data da Solicitação</th>
                    <th>Status</th>
                    <th>Data Início</th>
                    <th>Data Fim</th>
                    <th>Qtde Dias</th>
                    <th>Chefe imediato</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($solicitacoes as $s)
                <tr>
                    <td>{{$s->id}}</td>
                    <td>{{$s->getreferencia()}}</td>
                    <td>{{date('d/m/Y H:i', strtotime( $s->created_at))}}</td>
                    <td> -- Aguardando Análise --</td>
                    <td>{{date('d/m/Y', strtotime( $s->data_inicial))}}</td>
                    <td>{{date('d/m/Y', strtotime( $s->data_final))}}</td>
                    <td>{{App\Utils\DateUtil::qtdDias($s->data_inicial,  $s->data_final)}}</td>

                    <td>{{$s->superior?$s->superior->matricula . ' - ' . $s->superior->nome : ''}}</td>
                    <td>

                        <div class="dropdown open">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown">
                                Selecione
                            </button>
                            <div class="dropdown-menu" aria-labelledby="triggerId">

                                @if($s->status_id == 3)
                                    <a class="dropdown-item" href="{{action('FeriasAvaliacaoRHController@edit', $s->id)}}">Avaliar</a>
                                @endif

                                <a class="dropdown-item" target="_blank" href="{{route('historicoavaliador',[2, $s->id])}}">Visualizar Solicitação</a>

                            </div>
                        </div>

                    </td>
                </tr>
                @endforeach

                </tbody>
</table>
