<table class="table table-striped table-bordered" id="tbSolFerias">
                <thead>
                <tr class="bg-dark text-white text-center">
                    <th>Id</th>
                    <th>REF</th>
                    <th>Data da Solicitação</th>
                    <th>Status</th>
                    <th>Data Início</th>
                    <th>Data Fim</th>
                    <th>Qtde Dias</th>
                    <th>Avaliador RH</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($solicitacoesrh as $s)
                <tr>
                    <td>{{$s->id}}</td>
                    <td>{{$s->getreferencia()}}</td>
                    <td>{{date('d/m/Y H:i', strtotime( $s->created_at))}}</td>
                    <td>{{$s->getavaliacao()}} </td>
                    <td>{{date('d/m/Y', strtotime( $s->data_inicial))}}</td>
                    <td>{{date('d/m/Y', strtotime( $s->data_final))}}</td>
                    <td>{{App\Utils\DateUtil::qtdDias($s->data_inicial,  $s->data_final)}}</td>

                    <td>{{$s->avaliador_id? $s->getAvaliador($s->avaliador_id)->matricula . ' - ' . $s->getAvaliador($s->avaliador_id)->nome : '' }} {{--$s->superior?$s->superior->matricula . ' - ' . $s->superior->nome : ''--}}</td>
                    <td>

                        <div class="dropdown open">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown">
                                Selecione
                            </button>
                            <div class="dropdown-menu" aria-labelledby="triggerId">

                                <a class="dropdown-item" target="_blank" href="{{route('historicoavaliador',[2, $s->id])}}">Visualizar Avaliação RH</a>
                                <a class="dropdown-item" target="_blank" href="{{route('historico',[$s->id])}}">Visualizar Todas as Avaliações</a>
                            </div>
                        </div>

                    </td>
                </tr>
                @endforeach

                </tbody>
</table>
