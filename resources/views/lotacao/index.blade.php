@extends('layouts.pagina')

@section('content')

<div class="col-12">

@include('messages.alert')

        <div class="text-center col-12">
                <a href="{{route('lotacao.create')}}" class="btn btn-large btn-primary m-3">
                    <b>Nova Lotação</b>
                </a>
            </div>

            <table class="table table-striped table-bordered" id="tbLotacao">
                <thead>
                <tr class="bg-dark text-white text-center">
                    <th>Id</th>
                    <th>Código</th>
                    <th>Descrição</th>
                    <th>Status</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($lotacoes as $l)
                <tr>
                    <td>{{$l->id}}</td>
                    <td>{{$l->codigo}}</td>
                    <td>{{$l->descricao}}</td>
                    <td>{{$l->deleted_at == NULL ? 'Ativo':'Inativo' }}</td>
                    <td>

                        <div class="dropdown open">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown">
                                        Selecione
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="triggerId">

                                        @if(!$l->deleted_at)
                                        <a class="dropdown-item" href="{{action('LotacaoController@edit', $l)}}">Editar</a>

                                        <a class="dropdown-item"
                                        onclick="return confirm('Confirmar inativar')"
                                        href="{{url('lotacao/remove/'.$l->id)}}">Inativar</a>

                                        @else

                                        <a class="dropdown-item" href="{{action('LotacaoController@ativar', $l->id)}}">Ativar</a>

                                        @endif

                                    </div>
                                </div>
                    </td>
                </tr>
                @endforeach

                </tbody>
            </table>

</div>

    <script>
        $(document).ready(function () {
            $('#tbLotacao').DataTable({
                "searching": true,
                "aaSorting": [[2, "asc"]],
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
                },
                "columnDefs": [{
                    "targets": [-1],
                    "orderable": false
                },
                {
                "targets": [0],
                "visible": false
                }],

            });
        });
    </script>
@endsection
