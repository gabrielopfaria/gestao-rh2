@extends ('layouts.pagina')
@section('content')
    <div class="col-12 mt-5 text-center">
        <h2> Dados Pessoais </h2>
    </div>
    <form action="{{ route('funcionario.store') }}" method="post" class="form-row">
        {{ csrf_field() }}
        <div class="col-2">
            <label>Matrícula</label>
            <input type="text" id="matricula" name="matricula" class="form-control matricula text-uppercase">
            <input type="text" id="superior_id" value="" name="superior_id" class="form-control matricula text-uppercase" hidden>
        </div>
        <div class="col-10">
            <label>Nome</label>
            <input type="text" id="nome" name="nome" class="form-control" value="" placeholder="Digite o nome completo">
        </div>


        <div class="col-4">
            <label>Sexo</label>
            <select name="sexo" id="sexo" class="form-control" required>
                <option value="" selected>-- Selecione --</option>
                <option value="MASCULINO">Masculino</option>
                <option value="FEMININO">Feminino</option>
            </select>
        </div>
        <div class="col-4">
            <label>Data de Nascimento</label>
            <input type="date" name="data_nascimento" id="data_nascimento" class="form-control" required>
        </div>
        <div class="col-4">
            <label>Natural de:</label>
            <select name="naturalidade_id" id="naturalidade_id" class="form-control" required>
                <option value="" selected>-- Selecione --</option>
                @foreach ($naturalidade as $n)
                    <option value="{{ $n->id }}">{{ $n->descricao }}</option>
                @endforeach
            </select>
        </div>


        <div class="col-4">
            <label>Nacionalidade</label>
            <input type="text" name="nacionalidade" id="nacionalidade" class="form-control" required>
        </div>
        <div class="col-4">
            <label>RG</label>
            <input type="text" name="rg" class="form-control" required>
        </div>
        <div class="col-4">
            <label>CPF</label>
            <input type="text" name="cpf" id="cpf" class="cpf form-control" required>
        </div>


        <div class="col-12 text-center mt-4">
            <h2>Dados Funcionais</h2>
        </div>
        <div class="col-2">
            <label>Data de Admissão</label>
            <input type="date" name="data_admissao" class="form-control" required>
        </div>
        <div class="col-2">
            <label>Vínculo</label>
            <select name="vinculo_id" id="vinculo_id" class="form-control" required>
                <option value="" selected>-- Selecione --</option>
                @foreach ($vinculo as $v)
                    <option value="{{ $v->id }}">{{ $v->sigla }} - {{ $v->descricao }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-5"> {{-- VERIFICAR COM O RICARDO SOBRE CADASTRO DO ID EM LOTACAO --}}
            <label>Lotação Prodam</label>
            <select name="lotacao_atual_id" id="lotacao_atual_id" class="form-control" required>
                <option value="">-- Selecione --</option>
                @foreach ($lotacao_prodam as $lp)
                    <option value="{{ $lp->id }}">{{ $lp->codigo }} - {{ $lp->descricao }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-3">
            <label>Lotação</label>
            <select name="lotacao_generico_id" id="lotacao_generico_id" class="form-control" required>
                <option value="">-- Selecione --</option>
                @foreach ($lotacao as $l)
                    <option value="{{ $l->id }}">{{ $l->descricao }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-4">
            <label>Cargo Prodam</label>
            <select name="cargo_id" id="cargo_id" class="form-control" required>
                <option value="" selected>-- Selecione --</option>
                @foreach ($cargo_prodam as $cp)
                    <option value="{{ $cp->id }}">{{ $cp->codigo }} - {{ $cp->descricao }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-4">
            <label>Cargo</label>
            <select name="cargo_generico_id" id="cargo_generico_id" class="form-control" required>
                <option value="" selected>-- Selecione --</option>
                @foreach ($cargo as $c)
                    <option value="{{ $c->id }}">{{ $c->cargo }}</option>
                @endforeach
            </select>
        </div>


        <div class="col-4">
            <label>Função</label>
            <select name="funcao_id" id="funcao_id" class="form-control" required>
                <option value="" selected>-- Selecione --</option>
                @foreach ($funcao as $f)
                    <option value="{{ $f->id }}">{{ $f->descricao }}</option>
                @endforeach
            </select>
        </div>

        <div class="col-3">
            <label>Horário de trabalho</label>
            <select name="horario_trabalho_id" id="horario_trabalho_id" class="form-control" required>
                <option value="" selected>-- Selecione --</option>
                @foreach ($horario_trabalho as $ht)
                    <option value="{{ $ht->id }}">{{ $ht->descricao }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-3">
            <label>Turno</label>
            <select name="turno_id" id="turno_id" class="form-control" required>
                <option value="" selected>-- Selecione --</option>
                @foreach ($turno as $t)
                    <option value="{{ $t->id }}">{{ $t->descricao }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-6">
            <label>Situação</label>
            <select name="situacao_id" id="situacao_id" class="form-control" required>
                <option value="" selected>-- Selecione --</option>
                @foreach ($situacao as $s)
                    <option value="{{ $s->id }}"> {{ $s->sigla }} - {{ $s->descricao }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-12">
            <label>Observação</label>
            <textarea class="form-control" rows="5" name="observacao" id="observacao" maxlength="2000"></textarea>
        </div>
        <div class="form-group form-footer col-12 mt-4">
            <div>
                <button class="btn btn-primary" type="submit" tabindex="34">Salvar</button>
                <a class="btn btn-default" href="{{route('home')}}">
                    Cancelar
                </a>
            </div>
        </div>
    </form>
@endsection
