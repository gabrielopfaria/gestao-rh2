@extends('layouts.pagina')

@section('content')
    <div class="col-12">
        @include('messages.alert')
        <div class="text-center col-12">
            <a href="{{ route('funcionario.create') }}" class="btn btn-large btn-primary m-3">
                <b>Novo Colaborador</b>
            </a>
        </div>

        <div class="col-12">
            <lista-funcionario :titulos="['MATRICULA','NOME','CPF','LOTAÇÃO ATUAL','AÇÃO']"
                :funcionarios="{{$funcionarios->toJson()}}"
                editar="/funcionario">
            </lista-funcionario>
        </div>


        {{$funcionarios->links()}}
    </div>
@endsection
