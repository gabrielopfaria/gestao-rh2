@extends('layouts.pagina')

@section('content')
    <div class="col-12">
        <div class="text-center col-12">
            <a href="{{ route('funcionario.create') }}" class="btn btn-large btn-primary m-3">
                <b>Novo Colaborador</b>
            </a>
        </div>
    </div>

    {{-- <form action="{{ route('funcionario.index') }}" method="get">
        <div class="input-group mb-3">
            <input type="search" placeholder="Busque aqui" value="{{ isset($busca) ? $busca : '' }}" id="busca"
                name="busca" class="busca form-control">
            <div class="input-group-append">
                <button class="btn btn-primary mb-3" type="submit" id="button-addon2">
                    Localizar
                </button>
            </div>
    </form>
    </div> --}}


    <table class="table table-striped table-bordered" id="tbFuncionario">
        <thead>
            <tr>
                {{-- <th>Id</th> --}}
                <th>Matrícula</th>
                <th>Nome</th>
                <th>CPF</th>
                <th>Lotação Atual</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($funcionarios as $f)
                <tr>
                    <td class="matricula">{{ $f->matricula }}</td>
                    <td>{{strtoupper($f->nome)}}</td>
                    <td class="cpf">{{ $f->cpf }}</td>
                    <td>{{$f->lotacao->descricao}}</td>
                    <td>
                        <div class="dropdown open">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="triggerId"
                                data-toggle="dropdown">
                                Selecione
                            </button>
                            <div class="dropdown-menu" aria-labelledby="triggerId">
                                <a class="dropdown-item" href="{{ action('FuncionarioController@edit', $f) }}">Editar</a>
                            </div>
                        </div>
                    </td>
            @endforeach
            </tr>
        </tbody>
    </table>
    {{-- {{ $funcionarios->links() }} --}}
    <script>

    </script>

@endsection
