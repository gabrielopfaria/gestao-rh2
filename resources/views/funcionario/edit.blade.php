@extends ('layouts.pagina')
@section('content')
    <div class="col-12 mt-5 text-center">
        <h2> Dados Pessoais </h2>
    </div>
    <form action="{{route('funcionario.update',$funcionario)}}" method="post" class="form-row">
        <input type="hidden" name="_method" value="put">
        {{ csrf_field() }}
        <div class="col-2">
            <label>Matrícula</label>
            <input type="text" value="{{$funcionario->matricula}}" id="matricula" name="matricula" class="form-control matricula text-uppercase">
        </div>
        <div class="col-10">
            <label>Nome</label>
            <input type="text" value="{{strtoupper($funcionario->nome)}}" id="nome" name="nome" class="form-control text-uppercase" value="" placeholder="Digite o nome completo">
        </div>
        <div class="col-6">
            <label>Sexo</label>
            <select name="sexo" id="sexo" class="form-control" required>
                <option value="">-- Selecione --</option>
                <option value="MASCULINO" {{$funcionario->sexo == 'MASCULINO'?"selected":""}}>Masculino</option>
                <option value="FEMININO" {{$funcionario->sexo == 'FEMININO'?"selected":""}}>Feminino</option>
            </select>
        </div>


        <div class="col-3">
            <label>Data de Nascimento</label>
            <input type="date" name="data_nascimento" value="{{$funcionario->data_nascimento}}"id="data_nascimento" class="form-control" required>
        </div>
        <div class="col-3">
            <label>Idade</label>
            <input type="text" id="idade" name="idade" class="form-control" value="{{$funcionario->idade}}" readonly>
        </div>
        <div class="col-3">
            <label>Natural de:</label>
            <select name="naturalidade_id" id="naturalidade_id" class="form-control" required>
                <option value="" selected>-- Selecione --</option>
                @foreach ($naturalidade as $n)
                    <option value="{{ $n->id }}"{{$n->id == $funcionario->naturalidade_id?"selected":""}}>{{ $n->descricao }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-3">
            <label>Nacionalidade</label>
            <input type="text" value="{{strtoupper($funcionario->nacionalidade)}}" name="nacionalidade" id="nacionalidade" class="form-control text-uppercase" required>
        </div>

        <div class="col-3">
            <label>RG</label>
            <input type="text" value="{{$funcionario->rg}}" name="rg" class="form-control" required>
        </div>

        <div class="col-3">
            <label>CPF</label>
            <input type="text" value="{{$funcionario->cpf}}" name="cpf" id="cpf" class="cpf form-control" required>
        </div>



        <div class="col-12 text-center mt-4">
            <h2>Dados Funcionais</h2>
        </div>
        <div class="col-2">
            <label>Data de Admissão</label>
            <input type="date" value="{{$funcionario->data_admissao}}" name="data_admissao" class="form-control" required>
        </div>
        <div class="col-2">
            <label>Vínculo</label>
            <select name="vinculo_id" id="vinculo_id" class="form-control" required>
                <option value="" selected>-- Selecione --</option>
                @foreach ($vinculo as $v)
                    <option value="{{ $v->id }}"{{$v->id == $funcionario->vinculo_id?"selected":""}}>{{ $v->sigla }} - {{ $v->descricao }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-5"> {{-- VERIFICAR COM O RICARDO SOBRE CADASTRO DO ID EM LOTACAO --}}
            <label>Lotação Prodam</label>
            <select name="lotacao_atual_id" id="lotacao_atual_id" class="form-control" required>
                <option value="">-- Selecione --</option>
                @foreach ($lotacao_prodam as $lp)
                    <option value="{{ $lp->id }}"{{$lp->id == $funcionario->lotacao_atual_id?"selected":""}}>{{ $lp->codigo }} - {{ $lp->descricao }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-3">
            <label>Lotação</label>
            <select name="lotacao_generico_id" id="lotacao_generico_id" class="form-control" required>
                <option value="">-- Selecione --</option>
                @foreach ($lotacao as $l)
                    <option value="{{ $l->id }}"{{$l->id == $funcionario->lotacao_generico_id?"selected":""}}>{{ $l->descricao }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-4">
            <label>Cargo Prodam</label>
            <select name="cargo_id" id="cargo_id" class="form-control" required>
                <option value="" selected>-- Selecione --</option>
                @foreach ($cargo_prodam as $cp)
                    <option value="{{ $cp->id }}"{{$cp->id == $funcionario->cargo_id?"selected":""}}>{{ $cp->codigo }} - {{ $cp->descricao }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-4">
            <label>Cargo</label>
            <select name="cargo_generico_id" id="cargo_generico_id" class="form-control" required>
                <option value="" selected>-- Selecione --</option>
                @foreach ($cargo as $c)
                    <option value="{{ $c->id }}"{{$c->id == $funcionario->cargo_generico_id?"selected":""}}>{{ $c->cargo }}</option>
                @endforeach
            </select>
        </div>

        <div class="col-4">
            <label>Função</label>
            <select name="funcao_id" id="funcao_id" class="form-control" required>
                <option value="">-- Selecione --</option>
                @foreach ($funcao as $f)
                    <option value="{{$f->id}}"{{$f->id == $funcionario->funcao_id?"selected":""}}>{{ $f->descricao }}</option>
                @endforeach
            </select>
        </div>

        <div class="col-3">
            <label>Horário de trabalho</label>
            <select name="horario_trabalho_id" id="horario_trabalho_id" class="form-control" required>
                <option value="">-- Selecione --</option>
                @foreach ($horario_trabalho as $ht)
                    <option value="{{ $ht->id }}"{{$ht->id == $funcionario->horario_trabalho_id?"selected":""}}>{{ $ht->descricao }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-3">
            <label>Turno</label>
            <select name="turno_id" id="turno_id" class="form-control" required>
                <option value="">-- Selecione --</option>
                @foreach ($turno as $t)
                    <option value="{{ $t->id }}"{{$t->id == $funcionario->turno_id?"selected":""}}>{{ $t->descricao }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-6">
            <label>Situação</label>
            <select name="situacao_id" id="situacao_id" class="form-control" required>
                <option value="" selected>-- Selecione --</option>
                @foreach ($situacao as $s)
                    <option value="{{ $s->id }}"{{$s->id == $funcionario->situacao_id?"selected":""}}> {{ $s->sigla }} - {{ $s->descricao }}</option>
                @endforeach
            </select>
        </div>

        <div class="col-12">
            <label>Observação</label>
            <textarea class="form-control" rows="5" name="observacao" id="observacao" maxlength="2000">{{$funcionario->observacao}}</textarea>
        </div>
        <div class="form-group form-footer col-12 mt-4">
            <div>
                <button class="btn btn-primary" type="submit" tabindex="34">Salvar</button>
                <a class="btn btn-default" href="{{route('home')}}">
                    Cancelar
                </a>
            </div>
        </div>
    </form>
@endsection
