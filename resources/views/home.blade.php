@extends('layouts.pagina')

@section('content')
    <div class="col-12">
        @include('messages.alert')
        <div class="dashboard">
            Funcionários Cadastrados: {{$qtd}} <br>
        </div>
        <div class="row" style="background-color: #ececec; border-radius: 10px">
            {{-- Ficha Funcional --}}
            <div class="col-sm-3 col-lg-3 text-center mt-3 mb-5">
                <h3>Ficha <br> Funcional</h3>

                <a href="{{ route('funcionario.create') }}">
                    <div class="btn btn-block btn-primary">
                        Cadastro
                    </div>
                </a>

                <button type="button" class="btn btn-block mt-3 btn-primary" data-toggle="modal"
                    data-target="#exampleModalCenter">
                    Buscar Colaborador
                </button>

                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Buscar Funcionário</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('funcionario.index') }}" method="get">
                                    <div class="input-group mb-3">
                                        <input type="search" placeholder="MATRICULA,CPF ou NOME"
                                            value="{{ isset($busca) ? $busca : '' }}" id="busca" name="busca"
                                            class="busca form-control">

                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">Pesquisar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>





                <a href="#">
                    <div class="btn btn-block btn-primary mt-3 disabled">
                        Remoção
                    </div>
                </a>
                <a href="#">
                    <div class="btn btn-block btn-primary mt-3 disabled">
                        Anotação
                    </div>
                </a>

            </div>

            {{-- Lic. Médica e Atestado --}}
            <div class="col-sm-3 text-center mt-3">
                <h3>Lic. Médica e <br> Atestado</h3>

                <a href="#">
                    <div class="btn btn-block btn-primary disabled">
                        Cadastro
                    </div>
                </a>

                <a href="#">
                    <div class="btn btn-block btn-primary mt-3 disabled">
                        Encaminhamento à Junta Médica
                    </div>
                </a>

            </div>

            {{-- Lic. Interesse Particular --}}
            <div class="col-sm-3 text-center mt-3">
                <h3>Lic. Interesse<br> Particular</h3>

                <a href="#">
                    <div class="btn btn-block btn-primary disabled">
                        Cadastro
                    </div>
                </a>

                <a href="#">
                    <div class="btn btn-block btn-primary disabled mt-3">
                        Lançamentos
                    </div>
                </a>

            </div>

            {{-- Lic. Especial --}}
            <div class="col-sm-3 text-center mt-3">
                <h3>Lic.<br>Especial</h3>

                <a href="#">
                    <div class="btn btn-block btn-primary disabled">
                        Cadastro
                    </div>
                </a>

                <a href="#">
                    <div class="btn btn-block btn-primary disabled mt-3">
                        Lançamentos
                    </div>
                </a>

            </div>
        </div>
    </div>
@endsection
