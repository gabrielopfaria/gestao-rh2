@extends('layouts.pagina')

@section('content')

<div class="col-12">

@include('messages.alert')

        <div class="text-center col-12">
                <a href="{{route('tipopagamento.create')}}" class="btn btn-large btn-primary m-3">
                    <b>Novo Tipo de Pagamento</b>
                </a>
            </div>

            <table class="table table-striped table-bordered" id="tbTipoPagamento">
                <thead>
                <tr class="bg-dark text-white text-center">
                    <th>Id</th>
                    <th>Código</th>
                    <th>Descrição</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tipopagamento as $t)
                <tr>
                    <td>{{$t->id}}</td>
                    <td>{{$t->codigo}}</td>
                    <td>{{$t->descricao}}</td>
                    <td>

                        <div class="dropdown open">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown">
                                        Selecione
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="triggerId">
                                        <a class="dropdown-item" href="{{action('TipoPagamentoController@edit', $t)}}">Editar</a>
                                        <a class="dropdown-item"
                                        onclick="return confirm('Confirmar exclusão')"
                                        href="{{url('tipopagamento/remove/'.$t->id)}}">Excluir</a>
                                    </div>
                                </div>
                    </td>
                </tr>
                @endforeach

                </tbody>
            </table>

</div>

    <script>
        $(document).ready(function () {
            $('#tbTipoPagamento').DataTable({
                "searching": true,
                "aaSorting": [[2, "asc"]],
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
                },
                "columnDefs": [{
                    "targets": [-1],
                    "orderable": false
                },
                {
                "targets": [0],
                "visible": false
                }],

            });
        });
    </script>
@endsection
