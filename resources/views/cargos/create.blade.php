@extends('layouts.pagina')
@section('content')
    <div class="col-12">
        <h1>
            Novo cargo
        </h1>
        @include('messages.alert')
    </div>
    <div class="col-12">
        <form action="{{route('cargo.store')}}" method="post" class="form-row">
            @csrf
            <div class="form-group col-4">
                <label class="font-weight-bold">Código</label>
                <input type="text" name="codigo" id="codigo" class="form-control" placeholder="">
            </div>
            <div class="form-group col-8">
                <label class="font-weight-bold">Descrição</label>
                <input type="text" name="descricao" id="descricao" class="form-control" placeholder="">
            </div>
            <div class="col-12 text-center">
                <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
        </form>
    </div>

@endsection
