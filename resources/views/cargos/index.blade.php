@extends('layouts.pagina')
@section('content')
<div class="col-12">
    <h1>Cargos</h1>
</div>
<div class="col-12 text-center">
    <a class="btn btn-primary my-3" href="{{route('cargo.create')}}" role="button">
        Novo cargo
    </a>
    @include('messages.alert')
    <table class="table table-bordered">
        <thead>
            <tr class="bg-dark text-white text-center">
                <th>Código</th>
                <th>Descrição</th>
                <th width="10%">Ações</th>
            </tr>
        </thead>
        <tbody>
            @foreach($cargos as $c)
            <tr>
                <td>{{$c->codigo}}</td>
                <td>{{$c->cargo}}</td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-dark dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown">
                            Selecionar
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{route('cargo.edit',$c)}}">Editar</a>
                            <!-- <a class="dropdown-item" href="#">Excluir</a> -->
                        </div>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
