<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gatas')->insert([
            [
                'nivel' => 'Nível 01',
                'valor' => 250
            ],
            [
                'nivel' => 'Nível 02',
                'valor' => 320
            ],
            [
                'nivel' => 'Nível 03',
                'valor' => 395
            ],
            [
                'nivel' => 'Nível 04',
                'valor' => 485
            ],
            [
                'nivel' => 'Nível 05',
                'valor' => 575
            ],
            [
                'nivel' => 'Nível 06',
                'valor' => 725
            ],
            [
                'nivel' => 'Nível 07',
                'valor' => 950
            ],
            [
                'nivel' => 'Nível 08',
                'valor' => 1145
            ],
            [
                'nivel' => 'Nível 09',
                'valor' => 1296
            ],
            [
                'nivel' => 'Nível 10',
                'valor' => 1508
            ],
            [
                'nivel' => 'Nível 11',
                'valor' => 1810
            ],
            [
                'nivel' => 'Nível 12',
                'valor' => 2206
            ],
            [
                'nivel' => 'Nível 13',
                'valor' => 2806
            ],
        ]);
    }
}
