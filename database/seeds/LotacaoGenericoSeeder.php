<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LotacaoGenericoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::unprepared(file_get_contents('database/sqls/lotacao_genericos.sql'));
    }
}
