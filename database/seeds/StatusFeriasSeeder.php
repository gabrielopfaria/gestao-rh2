<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusFeriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('status_ferias')->insert([
            'id' => '1',
            'status' => 'Aguardando envio para Aprovação.'
        ]);

        DB::table('status_ferias')->insert([
            'id' => '2',
            'status' => 'Enviado para aprovação do chefe imediato.'
        ]);

        DB::table('status_ferias')->insert([
            'id' => '3',
            'status' => 'Enviado para aprovação do RH.'
        ]);
        // DB::table('status_ferias')->insert([
        //     'id' => '3',
        //     'status' => 'Deferido pelo chefe imediato.'
        // ]);

        DB::table('status_ferias')->insert([
            'id' => '4',
            'status' => 'Indeferido pelo chefe imediato.'
        ]);

        DB::table('status_ferias')->insert([
            'id' => '5',
            'status' => 'Deferido pelo RH.'
        ]);

        DB::table('status_ferias')->insert([
            'id' => '6',
            'status' => 'Indeferido pelo RH.'
        ]);

    }
}
