<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HorarioTrabalhoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::unprepared(file_get_contents('database/sqls/horario_trabalhos.sql'));
    }
}
