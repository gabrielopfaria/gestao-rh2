<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoPagamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_pagamento')->insert([
            'codigo' => '1',
            'descricao' => 'VENCIMENTO'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '3',
            'descricao' => 'ADICIONAL TEMPO SERV'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '4',
            'descricao' => 'PROLABORE'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '25',
            'descricao' => 'SALÁRIO MATERNIDADE'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '40',
            'descricao' => 'ADICIONAL NOTURNO'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '45',
            'descricao' => 'GRATIFIC AUX MORADIA'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '47',
            'descricao' => 'SALÁRIO FAMILIA CLT'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '72',
            'descricao' => 'GRATIFIC ATIVIDADE'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '128',
            'descricao' => 'VENC.CONTRATO TEMPOR'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '147',
            'descricao' => 'ADICIONAL 1/3 FERIAS'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '335',
            'descricao' => 'DIF.CARGO COMISSION.'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '407',
            'descricao' => 'GRATIFICAÇÃO SAÚDE'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '578',
            'descricao' => 'VENC.TEMPORAR.40 HS'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '680',
            'descricao' => 'VENC.TEMPOR.36 HORAS'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '682',
            'descricao' => 'ADIC.NOT.M.ANTERIOR.'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '714',
            'descricao' => 'SAL.MATERN.PATRONAL'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '755',
            'descricao' => 'DIF.CONTR.TEMPORARIO'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '836',
            'descricao' => 'VENC COMIS C/VINCULO'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '841',
            'descricao' => 'VENC COMIS C/VINCULO'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '843',
            'descricao' => 'VENC COMIS S/VINCULO'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '848',
            'descricao' => 'GRAT REDE ASS S/VINC'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '895',
            'descricao' => 'DIF.AUX.ALIMENTACAO'
        ]);

        DB::table('tipo_pagamento')->insert([
            'codigo' => '22',
            'descricao' => 'RISCO DE VIDA'
        ]);
    }
}
