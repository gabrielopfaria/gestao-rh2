<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GrauParentescoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grau_parentesco')->insert([
            'descricao' => 'Pai'
        ]);

        DB::table('grau_parentesco')->insert([
            'descricao' => 'Mãe'
        ]);

        DB::table('grau_parentesco')->insert([
            'descricao' => 'Avô(ó)'
        ]);

        DB::table('grau_parentesco')->insert([
            'descricao' => 'Filho(a)'
        ]);

        DB::table('grau_parentesco')->insert([
            'descricao' => 'Neto(a)'
        ]);

        DB::table('grau_parentesco')->insert([
            'descricao' => 'Tio(a)'
        ]);

        DB::table('grau_parentesco')->insert([
            'descricao' => 'Sobrinho(a)'
        ]);

        DB::table('grau_parentesco')->insert([
            'descricao' => 'Primo(a)'
        ]);

        DB::table('grau_parentesco')->insert([
            'descricao' => 'Irmão(ã)'
        ]);

        DB::table('grau_parentesco')->insert([
            'descricao' => 'Sogro(a)'
        ]);

        DB::table('grau_parentesco')->insert([
            'descricao' => 'Genro'
        ]);

        DB::table('grau_parentesco')->insert([
            'descricao' => 'Nora'
        ]);

        DB::table('grau_parentesco')->insert([
            'descricao' => 'Cunhado(a)'
        ]);

        DB::table('grau_parentesco')->insert([
            'descricao' => 'Padrasto'
        ]);

        DB::table('grau_parentesco')->insert([
            'descricao' => 'Madrasta'
        ]);

        DB::table('grau_parentesco')->insert([
            'descricao' => 'Enteado(a)'
        ]);

    }
}
