<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [//funcionario
                'name' => 'Gabriel Omena Pantoja Faria',
                'cpf' => '94800588200',
                'email' => 'gabrielopfaria@gmail.com',
                'password' => Hash::make('123456789')
            ], [//chefe
                'name' => 'Ricardo Coimbra da Silva ',
                'cpf' => '69367639287',
                'email' => 'ricardp@ses.com',
                'password' => Hash::make('123456789')
            ], [//rh
                'name' => 'Gentil Pereira',
                'cpf' => '12345678900',
                'email' => 'gentil@rh.com',
                'password' => Hash::make('123456789')
            ]
        ]);
        // $this->call(BancoSeeder::class); //ok
        // $this->call(EscolaridadeSeeder::class); //ok
        $this->call(LotacaoSeeder::class);
        $this->call(LotacaoGenericoSeeder::class);
        $this->call(HorarioTrabalhoSeeder::class);
        $this->call(TurnoSeeder::class);
        $this->call(VinculoSeeder::class);
        $this->call(CargoGenericoSeeder::class);
        $this->call(CargoSeeder::class);
        $this->call(FuncaoSeeder::class);
        $this->call(NaturalidadeSeeder::class);
        // $this->call(GataSeeder::class); //ok
        // $this->call(CboSeeder::class); //ok
        $this->call(StatusFeriasSeeder::class);
        $this->call(SituacaoSeeder::class);
        // $this->call(EstadosSeeder::class);


        /*
        $this->call(TipoPagamentoSeeder::class);
        $this->call(SituacoesFeriasSeeder::class); //ok
       $this->call(GrauParentescoSeeder::class);
      */
    }
}
