<?php

use App\ApiUser;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ApiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ApiUser::create([
            'nome'=>'default',
            'api_token'=>Str::random(80)
        ]);
    }
}
