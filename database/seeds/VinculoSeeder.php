<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VinculoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vinculos')->insert([
            [
                'sigla' => 'A',
                'descricao' => 'ESTATUTARIO',
            ],
            [
                'sigla' => 'F',
                'descricao' => 'SERV.TEMPORARIO',
            ],
            [
                'sigla' => 'D',
                'descricao' => 'COMISSIONADO',
            ],
            [
                'sigla' => 'G',
                'descricao' => 'COMISSIONADO',
            ],
            [
                'sigla' => 'J',
                'descricao' => 'COMISSIONADO',
            ],
        ]);
    }
}
