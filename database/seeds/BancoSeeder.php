<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class BancoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bancos')->insert([
            'codigo' => '001',
            'descricao' => 'Banco Do Brasil S.A (BB)'
        ]);

        DB::table('bancos')->insert([
            'codigo' => '237',
            'descricao' => 'Bradesco S.A.'
        ]);

        DB::table('bancos')->insert([
            'codigo' => '260',
            'descricao' => 'Nu Pagamentos S.A (Nubank)'
        ]);

        DB::table('bancos')->insert([
            'codigo' => '290',
            'descricao' => 'PagSeguro Internet S.A.'
        ]);

        DB::table('bancos')->insert([
            'codigo' => '323',
            'descricao' => 'Mercado Pago – Conta Do Mercado Livre'
        ]);

        DB::table('bancos')->insert([
            'codigo' => '341',
            'descricao' => 'Itaú Unibanco S.A.'
        ]);

        DB::table('bancos')->insert([
            'codigo' => '104',
            'descricao' => 'Caixa Econômica Federal (CEF)'
        ]);

        DB::table('bancos')->insert([
            'codigo' => '033',
            'descricao' => 'Banco Santander Brasil S.A.'
        ]);

        DB::table('bancos')->insert([
            'codigo' => '003',
            'descricao' => 'Banco Da Amazônia S.A.'
        ]);

        DB::table('bancos')->insert([
            'codigo' => '394',
            'descricao' => 'Banco Bradesco Financiamentos S.A.'
        ]);

        DB::table('bancos')->insert([
            'codigo' => '007',
            'descricao' => 'BNDES (Banco Nacional Do Desenvolvimento Social)'
        ]);
    }
}
