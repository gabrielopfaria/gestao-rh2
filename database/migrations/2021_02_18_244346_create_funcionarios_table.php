<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFuncionariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funcionarios', function (Blueprint $table) {
            $table->bigIncrements('id');

            //Dados Pessoais
            $table->string('matricula');
            $table->string('nome');
            $table->string('sexo');
            $table->date('data_nascimento');
            $table->string('nacionalidade')->nullable();

            $table->bigInteger('naturalidade_id')->unsigned()->nullable();
            $table->foreign('naturalidade_id')
                ->references('id')->on('naturalidades');

            $table->string('cpf');
            $table->string('rg')->nullable();

            //Dados Funcionais
            $table->bigInteger('situacao_id')->unsigned();
            $table->foreign('situacao_id')
                ->references('id')->on('situacoes');

            $table->date('data_admissao');

            $table->bigInteger('lotacao_atual_id')->unsigned();
            $table->foreign('lotacao_atual_id')
                ->references('id')->on('lotacoes');

            $table->bigInteger('lotacao_generico_id')->unsigned();
            $table->foreign('lotacao_generico_id')
                ->references('id')->on('lotacao_genericos');

            $table->bigInteger('cargo_id')->unsigned();
            $table->foreign('cargo_id')
                ->references('id')->on('cargos');

            $table->bigInteger('cargo_generico_id')->unsigned();
            $table->foreign('cargo_generico_id')
                ->references('id')->on('cargo_genericos');

            $table->bigInteger('funcao_id')->unsigned()->nullable();
            $table->foreign('funcao_id')
                ->references('id')->on('funcoes');

            $table->bigInteger('horario_trabalho_id')->unsigned();
            $table->foreign('horario_trabalho_id')
                ->references('id')->on('horario_trabalhos');

            $table->bigInteger('turno_id')->unsigned();
            $table->foreign('turno_id')
                ->references('id')->on('turnos');

            $table->bigInteger('vinculo_id')->unsigned();
            $table->foreign('vinculo_id')
                ->references('id')->on('vinculos');

            $table->bigInteger('superior_id')->unsigned()->nullable()->comment('Chefe imediato');
            $table->foreign('superior_id')
                ->references('id')->on('funcionarios');

            $table->string('observacao')->nullable();

            $table->timestamps();

            // $table->bigInteger('rg_uf_id')->unsigned()->nullable();
            // $table->foreign('rg_uf_id')
            //     ->references('id')->on('estados');
            // $table->string('pis');
            // $table->string('titulo_eleitor');
            // $table->bigInteger('escolaridade_id')->unsigned()->nullable();
            // $table->foreign('escolaridade_id')
            //     ->references('id')->on('escolaridades');
            // $table->string('nome_mae')->nullable();
            // $table->string('nome_pai')->nullable();
            // $table->string('cep');
            // $table->string('cidade')->nullable();
            // $table->string('bairro')->nullable();
            // $table->string('endereco')->nullable();
            // $table->string('telefone')->nullable();
            // $table->string('email')->nullable();
            // $table->bigInteger('gata_id')->unsigned()->nullable();
            // $table->foreign('gata_id')
            //     ->references('id')->on('gatas');
            // $table->bigInteger('cbo_id')->unsigned();
            // $table->foreign('cbo_id')
            //     ->references('id')->on('cbo');
            // $table->date('data_situacao_inicial');
            // $table->date('data_situacao_final')->nullable()->nullable();
            // $table->string('diario')->nullable();
            // $table->date('data_diario')->nullable();
            // $table->bigInteger('banco_id')->unsigned();
            // $table->foreign('banco_id')
            //     ->references('id')->on('bancos');
            // $table->string('agencia');
            // $table->string('conta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funcionarios');
    }
}
