<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeriasSolicitacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ferias_solicitacao', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('funcionario_id')->unsigned();
            $table->foreign('funcionario_id')
                ->references('id')->on('funcionarios');

                $table->date('data_inicial');
                $table->date('data_final');

            //status fica nulo quando ainda está em momento
            $table->bigInteger('status_id')->unsigned()->nullable();
            $table->foreign('status_id')
                ->references('id')->on('status_ferias');

            $table->text('observacao')->nullable();

            $table->bigInteger('superior_id')->unsigned()->nullable()->comment('Chefe imediato no momento da solicitação.');
            $table->foreign('superior_id')
                ->references('id')->on('funcionarios');

            $table->bigInteger('user_id')->unsigned()->comment('Usuário que realizou solicitação');
            $table->foreign('user_id')
                ->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ferias_solicitacao');
    }
}
