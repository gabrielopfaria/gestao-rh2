<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeriasAvaliacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ferias_avaliacao', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('solicitacao_id')->unsigned();
            $table->foreign('solicitacao_id')
                ->references('id')->on('ferias_solicitacao');

            $table->bigInteger('avaliador_id')->unsigned()->comment('Responsável pela deferimento/indeferimento da solicitação');
            $table->foreign('avaliador_id')
                ->references('id')->on('funcionarios');

            $table->boolean('status')->comment('0-Indeferido, 1-Deferido');

            $table->string('motivo_indeferido', '200')->nullable();

            $table->tinyInteger('tipo_avaliador')->comment('1-Superior, 2-RH');

            $table->bigInteger('user_id')->unsigned()->comment('Usuário que realizou a avaliação');
            $table->foreign('user_id')
                ->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ferias_avaliacao');
    }
}
