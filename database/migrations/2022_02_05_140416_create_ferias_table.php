<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ferias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('ano', '4')->nullable();
            $table->date('data_expedicao')->nullable();
            $table->integer('ano_aquisitivo_inicial')->nullable();
            $table->integer('ano_aquisitivo_final')->nullable();
            $table->date('data_inicial')->nullable();
            $table->date('data_final')->nullable();
            $table->date('data_retorno')->nullable();
            $table->integer('qtd_dias');
            $table->string('func_matricula','20');

            $table->bigInteger('funcionario_id')->unsigned();
            $table->foreign('funcionario_id')
                ->references('id')->on('funcionarios');

            $table->bigInteger('solicitacao_id')->unsigned()->nullable()->comment('NULL quando registro não solicitado pelo sistema');
            $table->foreign('solicitacao_id')
                ->references('id')->on('ferias_solicitacao');

            $table->bigInteger('user_id')->unsigned()->comment('Usuário que realizou lançamento');
            $table->foreign('user_id')
                ->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ferias');
    }
}
