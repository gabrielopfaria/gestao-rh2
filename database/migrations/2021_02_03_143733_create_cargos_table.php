<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCargosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo', '20');
            $table->string('descricao','200');
            $table->string('quadro','100')->nullable();

            $table->bigInteger('cargo_genericos_id')->unsigned()->nullable()->comment('Referente a que cargo');
            $table->foreign('cargo_genericos_id')
                ->references('id')->on('cargo_genericos');

            $table->timestamps();
            $table->softDeletes()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargos');
    }
}
