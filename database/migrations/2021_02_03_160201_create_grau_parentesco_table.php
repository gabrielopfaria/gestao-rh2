<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrauParentescoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('grau_parentesco', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('descricao','20');

        //     //nulo para seeder
        //     $table->bigInteger('usuario_id_fk')->unsigned()->nullable()->comment('Usuário que realizou lançamento');
        //     $table->foreign('usuario_id_fk')
        //         ->references('id')->on('users');

        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grau_parentesco');
    }
}
