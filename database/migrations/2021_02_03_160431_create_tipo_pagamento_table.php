<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipoPagamentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('tipo_pagamento', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('codigo', '10');
        //     $table->string('descricao', '100');

        //     //nulo quando criado por seeder ou importado
        //     $table->bigInteger('usuario_id_fk')->unsigned()->nullable()->comment('Usuário que realizou lançamento');
        //     $table->foreign('usuario_id_fk')
        //         ->references('id')->on('users');

        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_pagamento');
    }
}
